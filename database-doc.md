# Database Docs

## ERD

![](./docs/erd.PNG)

## Overview
- A Product Catalog has one-to-many Products
- A Product has one-to-many Features
- An Order has one-to-many Order Items
- One Feature has One Specification
- A Specification may have an additional cost associated with it
- The onhand products that are available and ready to ship are stored in the Inventory table
- Every Product has a unique Configuration, a selection of Features & Specifications
- A model number is a snapshot that represents a configuration of a product
- `view_available_models` displays all of the sellable unique configurations
- The Specifications that were purchased for an order item are stored in the `purchased_specification` table


## Tables

### product_catalog
The classification of a group of related products.
|Column                  |Description                                                         |
|------------------------|--------------------------------------------------------------------|
|product_catalog_id      |Primary key / auto increment                                        |
|catalog_name            |The classification of all the items in a catalog                    |
|is_active               |Used to indicate if this catalog should be displayed to the customer|

### product
All of the available products in the system.
|Column                  |Description                                                                 |
|------------------------|----------------------------------------------------------------------------|
|product_Id              |Primary key / auto increment                                                |
|product_catalog_id      |The product catalog that this product belongs to                            |
|product_desc            |The description of the product displayed to the customer                    |
|price                   |The price in which the product is currently being sold                      |
|make                    |The make of the product                                                     |
|model                   |The model of the product                                                    |
|active                  |A flag used to indicate if this product is visible to the customer          |

### order_item
A product order must contain at least one order item.
|Column                  |Description                                                                 |
|------------------------|----------------------------------------------------------------------------|
|order_item_id           |Primary key / auto increment                                                |
|product_order_id        |The order (parent table) that this order item belongs to                    |
|product_id              |The product associated with this order item                                 |
|qty                     |The number of products the customer ordered                                 |
|purchase_price          |The total price that the customer paid for each product                     |
|timestamp               |The time in which this order item was created in the database               |

### product_order
A customer order that contains one to many products.
|Column                      |Description                                                                 |
|----------------------------|----------------------------------------------------------------------------|
|product_order_id            |Primary key / auto increment                                                |
|order_status_id             |The ID that represents one of the states an order can be in                 |
|order_uuid                  |The public unique 8 character string that represents an order a customer placed|
|amount                      |The total amount of the order, the sum of all order items                   |
|shipping_name               |The name of the person that the order is being shipped to                   |
|shipping_address_1          |Street address for shipping                                                 |
|shipping_address_2          |Street address for shipping                                                 |
|shipping_city               |City for shipping                                                           |
|shipping_state              |State for shipping                                                          |
|shipping_zip                |Zip for shipping                                                            |
|order_date                  |The date in which this order was saved to the database                      |
|stripe_payment_intent_id    |The id of the Payment Intent that was sent to Stripe for this order.  The `product_order.order_uuid` is stored in Stripe as metadata.  |
|stripe_payment_intent_status|The payment status associated with the Payment Intent                       |
|shipping_tracking_info      |The UPS tracking code associated with this order                            |

### order_status
The status of the order as it gets processed in the system.
|Column                      |Description                                                                 |
|----------------------------|----------------------------------------------------------------------------|
|order_status_id             |Primary key / auto increment                                                |
|the_status                  |New \| Building \| Built \| Shipping \| Shipped \| Full Refund \| Partial Refund |

### product_feature_spec
The collection of all conceivable features and specifications for a product.
|Column                      |Description                                                                 |
|----------------------------|----------------------------------------------------------------------------|
|product_feature_spec_id     |Primary key / auto increment                                                |
|product_id                  |The id of the product                                                       |
|feature_id                  |The id of the feature                                                       |
|specification_id            |The id of the specification                                                 |

### feature
All of the conceivable features for a product.
|Column                      |Description                                                                 |
|----------------------------|----------------------------------------------------------------------------|
|feature                     |Primary key / auto increment                                                |
|feature_desc                |The description of this feature                                             |
|feature_value               |Reserved for future use                                                     |

### specification
A unique quality associated with a feature.
|Column                      |Description                                                                 |
|----------------------------|----------------------------------------------------------------------------|
|specification               |Primary key / auto increment                                                |
|feature_id                  |The feature that this specification is associated with                      |
|spec_desc                   |The description that describes this specification                           |
|spec_value                  |The numerical value associated with this specification (if any)             |
|cost                        |The cost associated with this specification (if any)                        |

### purchased_specification
The collection of specifications that a customer purchased for a product.
|Column                            |Description                                                               |
|----------------------------------|--------------------------------------------------------------------------|
|purchased_specification           |Primary key / auto increment                                              |
|order_item_id                     |The item in the order in the order that has spec associated with it       |
|specification_id                  |The specification that the customer has selected                          |

### available_product_feature_spec
A collection of products with features & specs that are in stock ready to ship.
|Column                            |Description                                                               |
|----------------------------------|--------------------------------------------------------------------------|
|available_product_feature_spec_id |Primary key / auto increment                                              |
|product_feature_spec_id           |The id associated with a feature & spec                                   |
|model_id                          |A "rollup" id that maps all the features and specs for a configuration    |
|showcase_order                    |A number used to indicate the importance of this model                    |

### inventory
The collection of specifications that a customer purchased for a product rolled up to a Model
|Column                  |Description                                                                               |
|------------------------|------------------------------------------------------------------------------------------|
|inventory_id            |Primary key / auto increment                                                              |
|qty                     |The number of products for this configuration that are available to ship now              |

### stripe_log
A simple log that holds events from the order process.
|Column                  |Description                                                                 |
|------------------------|----------------------------------------------------------------------------|
|stripe_log_id           |Primary key / auto increment                                                |
|order_uuid              |The unique id assigned to an order (may be NA)                              |
|ip                      |The IP address of the client                                                |
|message                 |The message to log                                                          |
|date_time               |The time this was saved to the database                                     |

### model
Stores the unique configuration associated with a set of features & specs.
|Column                  |Description                                                                 |
|------------------------|----------------------------------------------------------------------------|
|model_id                |Primary key / auto increment                                                |
|model_number            |11 character text code used to represent the model_id                       |
|notes                   |Reserved for adding any additional notes for this model                     |
|is_active               |Used to indicate if this model is available for purchase                    |

## Views
|Name                                  |Description                                                                 |
|--------------------------------------|----------------------------------------------------------------------------|
|view_product_feature_spec             |Shows all of the products and their POSSIBLE features & specifications      |
|view_available_models                 |Shows detailed information for products that are IN STOCK ready to ship     |
