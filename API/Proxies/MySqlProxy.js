//
// File: MySqlProxy.js
// Auth: Martin Burolla
// Date: 3/30/2022
// Desc: Wrapper around MySql database.
//

const cs = require('../ConnString')
const mysql = require('mysql2/promise')
const checkoutSQL = require('./CheckoutSQL')
const productCatalogSQL = require('./ProductCatalogSQL')

exports.promisePool = mysql.createPool(cs.connString)


//////////////////////////////////////////////////////////////////////////
// READS
//////////////////////////////////////////////////////////////////////////

exports.getModelsForSearch = async (modelIds) => {
    const modelString = modelIds.toString()
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_MODELS_SEARCH, [modelString]);
    return rows
}

exports.getFeaturesAndSpecs = async () => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_FEATURES_SPECS);
    return rows
}

exports.getOrderAmount = async (orderUuid) => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_ORDER_AMOUNT, [orderUuid]);
    return Number(rows[0].amount)
}

exports.getOrderItemsForOrder = async (orderUuid) => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_ORDER_ITEMS_FOR_ORDER, [orderUuid]);
    return rows
}

exports.getShippingInfo = async (orderUuid) => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_SHIPPING_INFO, [orderUuid]);
    return rows[0]
}

exports.getOrderItems = async (orderUuid) => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_ORDER_ITEMS, [orderUuid]);
    return rows
}

exports.getOrderWithPaymentIntent = async (orderUuid, paymentIntentId) => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_PRODUCT_ORDER_WITH_PAYMENT_INTENT, [orderUuid, paymentIntentId]);
    return rows[0]
}

exports.getPriceForOrder = async(orderUuid) => {
    let retval = null;
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_ORDER_PRICE, [orderUuid]);
    if (rows[0]) {
        retval = rows[0].amount.replace('.', '')
    } 
    return retval
}

exports.getProducts = async () => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_PRODUCTS);
    return rows;
}

exports.getProductsForSpecIds = async (featureId, specIds) => {
    const [rows] = await this.promisePool.query(checkoutSQL.SELECT_PRODUCTS_SEARCH, [featureId, specIds.toString()]);
    return rows;
}

//////////////////////////////////////////////////////////////////////////
// WRITES
//////////////////////////////////////////////////////////////////////////

exports.insertStripeLog = async (orderUuid, ip, message) => {
    await this.promisePool.query(checkoutSQL.INSERT_STRIPE_LOG, [orderUuid, ip, message]);
    return "ok"
}

exports.updateOrderWithPaymentIntent = async (paymentIntentId, orderUuid) => {
    await this.promisePool.query(checkoutSQL.UPDATE_PRODUCT_ORDER, [paymentIntentId, orderUuid]);
    return "ok"
}

exports.updateOrderWithPaymentStatus = async (orderUuid, paymentIntentId, paymentStatus) => {
    await this.promisePool.query(checkoutSQL.UPDATE_PRODUCT_ORDER_PAYMENT_STATUS, [paymentStatus, orderUuid, paymentIntentId]);
    return "ok"
}

//////////////////////////////////////////////////////////////////////////
// TRANSACTIONAL
//////////////////////////////////////////////////////////////////////////

// Creating an order...

exports.getTotalForOrder = async (conn, productOrderId) => {
    const [rows] = await conn.query(checkoutSQL.SELECT_TOTAL_FOR_ORDER, [productOrderId]);
    const retval = rows[0].total
    return Number(retval)
} 

exports.insertOrder = async (conn, shipping, userId) => {
    const [rows] = await conn.query(checkoutSQL.INSERT_PRODUCT_ORDER, [shipping.name, shipping.address.line1, shipping.address.line2, shipping.address.city, shipping.address.state, shipping.address.zip, userId]);
    return { 
        orderUuid: rows[1][0].order_uuid,
        productOrderId: rows[1][0].product_order_id
    }
}

exports.insertOrderItem = async (conn, productOrderId, modelId, qty, purchasePrice, make, model, totalSpecPrice, totalPrice) => {
    const [rows] = await conn.query(checkoutSQL.INSERT_ORDER_ITEM, [productOrderId, modelId, qty, purchasePrice, make, model, totalSpecPrice, totalPrice]);
    return rows[0].insertId
}

exports.updateProductOrderPrice = async (conn, productOrderId, price) => {
    await conn.query(checkoutSQL.UPDATE_PRODUCT_ORDER_PRICE, [price, productOrderId]);
    return "ok"
}

exports.insertPurchasedSpec = async (conn, orderItemId, specificationId, purchasedPrice) => {
    await conn.query(checkoutSQL.INSERT_PURCHASED_SPECIFICATION, [orderItemId, specificationId, purchasedPrice]);
    return "ok"
}

////////////////////////////////////////////////////////////////////////////////////////
// PRODUCT CATALOG
////////////////////////////////////////////////////////////////////////////////////////

exports.getProductCatalog = async () => {
    const [rows] = await this.promisePool.query(productCatalogSQL.SELECT_PRODUCT_CATALOG);
    return rows;
}

exports.getProductsAdmin = async (productCatalogId) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.SELECT_PRODUCTS, [productCatalogId]);
    return rows;
}

exports.getProductDetails = async (productCatalogId, productId) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.SELECT_PRODUCT_DETAILS, [productCatalogId, productId]);
    return rows;
}

exports.getFeatures = async () => {
    const [rows] = await this.promisePool.query(productCatalogSQL.SELECT_FEATURE);
    return rows;
}

exports.getSpecifications = async (featureId) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.SELECT_SPEC, [featureId]);
    return rows;
}

exports.addProduct = async (product) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.INSERT_PRODUCT, [
        product.productCatalogId,
        product.productDesc,
        product.price,
        product.make,
        product.model
    ]);
    return { ...product, productId: rows[0].insertId };
}

exports.addSpecForFeature = async (productId, specId, featureId) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.INSERT_FEATURE_SPEC, [productId, specId, featureId]);
    return rows[0].insertId
}

exports.addFeature = async (description) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.INSERT_FEATURE, [description]);
    return rows[0].insertId
}

exports.addSpecification = async (featureId, description, specValue, cost) => {
    const [rows] = await this.promisePool.query(productCatalogSQL.INSERT_SPEC, [featureId, description, specValue, cost]);
    return rows[0].insertId
}
