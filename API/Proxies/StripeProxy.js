//
// File: StripeProxy.js
// Auth: Martin Burolla
// Date: 10/28/2022
// Desc: The one and only interface into Stripe.
//       Note: The React app calls stripe.confirmPayment() which
//       charges the credit card for a Payment Intent.
//

const { stripeKey } = require('../App.config.js');
const stripe = require("stripe")(stripeKey)

exports.createPaymentIntent = async (orderUuid, totalAmount) => {
    let retval = null
    retval = await stripe.paymentIntents.create({
        amount: totalAmount,
        currency: "usd",
        automatic_payment_methods: { enabled: true },
        description: `Guitar Order (${new Date().toLocaleDateString()}) Order ID: ${orderUuid}`,
        statement_descriptor: "Guitar Store",
        metadata: { orderUuid: orderUuid }
    })
    return retval
}

exports.retrievePaymentIntent = async (paymentIntent) => {
    return await stripe.paymentIntents.retrieve(paymentIntent)
}
