//
// File: ProductCatalogSQL.js
// Auth: Martin Burolla
// Date: 12/19/2022
// Desc: All the SQL statements related to the secure Product Catalog.
//

exports.SELECT_PRODUCT_CATALOG = `
    select 
        product_catalog_id, 
        catalog_name,
        is_active
    from 
        product_catalog`

exports.SELECT_PRODUCTS = `
    select 
        *
    from 
        product
    where 
        product_catalog_id = ?;`

exports.SELECT_FEATURE = `
    select 
        *
    from 
        feature`

exports.SELECT_SPEC = `
    select 
    *
    from 
        specification
    where 
        feature_id = ?`

exports.SELECT_PRODUCT_DETAILS = ` 
    select   
        product_feature_spec_id,
        pfs.product_id,
        pfs.feature_id,
        pfs.specification_id,
        product_catalog_id,
        product_desc,
        price,
        make,
        model,
        feature_desc,
        feature_value,
        spec_desc, 
        spec_value,
        cost
    from 
        product_feature_spec pfs
        join product p on pfs.product_id = p.product_id
        join feature f on pfs.feature_id = f.feature_id
        join specification s on pfs.specification_id = s.specification_id
    where 
        active = 1 and 
        product_catalog_id = ? and 
        p.product_id = ?;`

exports.INSERT_PRODUCT = `
    insert into 
        product (product_catalog_id, product_desc, price, make, model, active) 
    values 
        (?, ?, ?, ?, ?, true);
    SELECT LAST_INSERT_ID();`

exports.INSERT_FEATURE_SPEC = `
    insert into 
        product_feature_spec (product_id, specification_id, feature_id)
    values 
        (?, ?, ?);
    SELECT LAST_INSERT_ID();`

exports.INSERT_FEATURE = `
    insert into 
        feature (feature_desc) 
    values (?);
    SELECT LAST_INSERT_ID();`

exports.INSERT_SPEC = `
    insert into 
        specification (feature_id, spec_desc, spec_value, cost) 
    values (?, ?, ?, ?);
    SELECT LAST_INSERT_ID();`
