//
// File: CheckoutSQL.js
// Auth: Martin Burolla
// Date: 12/19/2022
// Desc: All the SQL statements related to shopping and checkout.
//

exports.SELECT_MODELS_SEARCH = `
    select 
        *
    from 
        view_available_models
    where 
        FIND_IN_SET(model_id, ?)`

exports.SELECT_FEATURES_SPECS = `
    select 
        * 
    from 
        feature f 
        join specification s on f.feature_id = s.feature_id
    where 
    specification_id not in (1)
    order by 
    f.feature_id`

exports.SELECT_ORDER_AMOUNT = `
    select 
        amount 
    from 
        product_order 
    where 
        order_uuid = ?`

exports.SELECT_ORDER_PRICE = `
    select 
        amount 
    from 
        product_order 
    where 
        order_uuid = ?`

exports.SELECT_ORDER_ITEMS = `
    select 
        oi.order_item_id,
        oi.make,
        oi.model,
        (oi.total_spec_price + oi.purchase_price) as price_each,
        qty,
        oi.total_price
    from
        product_order po
        join order_item oi on oi.product_order_id = po.product_order_id
    where
        po.order_uuid = ?`

exports.SELECT_TOTAL_FOR_ORDER = `
    select 
        SUM(total_price) as total
    from 
        order_item 
    where 
        product_order_id = ?`

exports.SELECT_ORDER_ITEMS_FOR_ORDER = `
    select distinct 
        oi.order_item_id,
        oi.qty,
        v.make, 
        v.model
    from
        product_order po
        join order_item oi on oi.product_order_id = po.product_order_id 
        join view_available_models v on v.model_id = oi.model_id
    where 
        order_uuid = ?`

exports.SELECT_PRODUCTS = `
    select 
        product_id, 
        feature_id,
        specification_id,
        model_id,
        model_number,
        product_desc,
        feature_desc,
        spec_desc,
        cost,
        make, 
        model,
        price,
        qty,
        showcase_order
    from 
        view_available_models
    where 
        active = 1
    order by 
        showcase_order`

exports.SELECT_PRODUCTS_SEARCH = `
        select 
            product_id, 
            feature_id,
            specification_id,
            model_id,
            model_number,
            product_desc,
            feature_desc,
            spec_desc,
            cost,
            make, 
            model,
            price,
            qty
        from 
            view_available_models
        where 
            active = 1 and
            feature_id = ? and 
            FIND_IN_SET(specification_id, ?)
        order by 
            model_id`

exports.SELECT_SHIPPING_INFO = `
    select 
        shipping_name,
        shipping_address_1,
        shipping_address_2,
        shipping_city,
        shipping_state,
        shipping_zip
    from 
        product_order 
    where 
        order_uuid = ?`

exports.SELECT_PRODUCT_ORDER_WITH_PAYMENT_INTENT =  `
    select 
        po.order_uuid,
        po.amount,
        po.shipping_name,
        po.shipping_address_1,
        po.shipping_address_2,
        po.shipping_city,
        po.shipping_state,
        po.shipping_zip,
        po.stripe_payment_intent_status as pid_status,
        po.stripe_payment_intent_id as pid
    from 
        product_order po
    where 
        po.order_uuid = ? and 
        po.stripe_payment_intent_id = ?`

exports.INSERT_PRODUCT_ORDER = `
    insert into product_order (
        order_uuid,
        shipping_name,
        shipping_address_1, 
        shipping_address_2, 
        shipping_city, 
        shipping_state, 
        shipping_zip,
        user_id)
    values 
        (UPPER(SUBSTRING(uuid(), 1, 8)),?,?,?,?,?,?,?);
    SELECT order_uuid, product_order_id from product_order where product_order_id = LAST_INSERT_ID();`

exports.INSERT_ORDER_ITEM = `
    insert into order_item (
        product_order_id, 
        model_id, 
        qty, 
        purchase_price,
        make,
        model,
        total_spec_price,
        total_price) 
    values 
        (?, ?, ?, ?, ?, ?, ?, ?);
    SELECT LAST_INSERT_ID();`

exports.INSERT_PURCHASED_SPECIFICATION = `
    insert into purchased_specification (
        order_item_id, 
        specification_id, 
        purchased_price) 
    values 
        (?, ?, ?);
    SELECT LAST_INSERT_ID();`

exports.INSERT_STRIPE_LOG = `
    insert into 
        stripe_log (order_uuid, ip, message) 
    values (?, ?, ?)`

exports.UPDATE_PRODUCT_ORDER = `
    update 
        product_order 
    set 
        stripe_payment_intent_id = ? 
    where 
        order_uuid = ?`

exports.UPDATE_PRODUCT_ORDER_PAYMENT_STATUS = `
    update 
        product_order 
    set 
        stripe_payment_intent_status = ? 
    where 
        order_uuid = ? and 
        stripe_payment_intent_id = ? and
        isNull(stripe_payment_intent_status)`

exports.UPDATE_PRODUCT_ORDER_PRICE = `
    update 
        product_order
    set 
        amount = ?
    where
        product_order_id = ?`
