/* eslint-disable no-unused-vars */

//
// File: testMySqlProxy.js
// Auth: Martin Burolla
// Date: 10/27/2022
// Desc: Tests the MySqlProxy for database access.
//

const mySqlProxy = require('../Proxies/MySqlProxy')

const insertOrder = async () => {
    const shipping = {
        name: 'Alice in Chains',
        address: {
            line1: '722 Ontario Drive',
            line2: '',
            city: 'Ontario',
            state: 'NY',
            zip: '14519'
        }
    }
    const { orderUuid, productOrderId } = await mySqlProxy.insertOrder(shipping)
    console.log(orderUuid)
    console.log(productOrderId)
}

const getOrder = async () => {
    const orderUuid = "BBE21821"
    const res = await mySqlProxy.getOrder(orderUuid)
    console.log(res)
}

const insertOrderItem = async () => {
    const productOrderId = 9
    const productId = 1
    const qty = 1
    const purchasePrice = 1200.00
    const res = await mySqlProxy.insertOrderItem(productOrderId, productId, qty, purchasePrice)
    console.log(res)
}

const getProducts = async () => {
    const res = await mySqlProxy.getProducts()
    console.log(res)
}

const updateProductOrderPrice = async () => {
    const productOrderId = 29
    const price = 222
    const res = await mySqlProxy.updateProductOrderPrice(productOrderId, price)
    console.log(res)
}

const getShippingInfo = async () => {
    const res = await mySqlProxy.getShippingInfo('135FA360')
    console.log(res)
}

const getOrderItems = async () => {
    const res = await mySqlProxy.getOrderItems('135FA360')
    console.log(res)
}

const getOrderAmount = async () => {
    const res = await mySqlProxy.getOrderAmount('6973CB3B')
    console.log(res)
}


const main = async () => {
    await getOrderAmount()
    // await getProducts()
    // await getOrder()
    // await insertOrder()
    // await insertOrderItem()
    // await getOrderAmount()
    // await updateProductOrderPrice()
    // await getShippingInfo()
    // await getOrderItems()

    process.exit()
}

main()
