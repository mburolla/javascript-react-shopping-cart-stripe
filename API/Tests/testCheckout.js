//
// File: testCheckout.js
// Auth: Martin Burolla
// Date: 11/8/2022
// Desc: 
//

let co = require('../Checkout/Checkout')

const main = async () => {
    let inStockProducts = await co.getProducts()
    console.log(inStockProducts)
    process.exit()
}

main()
