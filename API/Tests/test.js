const assert = require('assert')
const order = require('../Checkout/Order')
const { findModelIds } = require('../Checkout/Search')
const mySqlProxy = require('../Proxies/MySqlProxy')

describe('Order', async () => {
  describe('Create order for one item', async () => {
    it('should be equal to $1,000.00', async () => {
        const items = [
          {
              modelId: 1, // 1 X $1,000 = $1,000
              qty: 1
          }
        ]
        const shipping = {
            name: "Martin J. Burolla",
            address: {
              line1: "123 Green Street",
              line2: "",
              city: "Ontario",
              state: "NY",
              zip: "14519-9343"
            }
        }
        const orderUuid = await order.createOrder(items, shipping)
        const theOrder = await mySqlProxy.getOrderAmount(orderUuid)
        assert.equal(theOrder, '1000.00')
        assert.equal(orderUuid.length, 8)
    })
  })
})

describe('Order', async () => {
  describe('Create order for two items', async () => {
    it('should be equal to $2,000.00', async () => {
        const items = [
          {
              modelId: 1, // 1 X $1,000 = $2,000
              qty: 2
          }
        ]
        const shipping = {
            name: "Martin J. Burolla",
            address: {
              line1: "123 Green Street",
              line2: "",
              city: "Ontario",
              state: "NY",
              zip: "14519-9343"
            }
        }
        const orderUuid = await order.createOrder(items, shipping)
        const theOrder = await mySqlProxy.getOrderAmount(orderUuid)
        assert.equal(theOrder, '2000.00')
        assert.equal(orderUuid.length, 8)
    })
  })
})

describe('Order', async () => {
  describe('Create order for three different items', async () => {
    it('should be equal to $6,000.00', async () => {
        const items = [
          {
            modelId: 1, // 1 X $1,000 = $1,000
            qty: 1
          },
          {
            modelId: 2, // 1 X $2,000 = $2,000
            qty: 1
          },
          {
            modelId: 3, // 1 X $3,000 = $3,000
            qty: 1
          }
        ]
        const shipping = {
            name: "Martin J. Burolla",
            address: {
              line1: "123 Green Street",
              line2: "",
              city: "Ontario",
              state: "NY",
              zip: "14519-9343"
            }
        }
        const orderUuid = await order.createOrder(items, shipping)
        const theOrder = await mySqlProxy.getOrderAmount(orderUuid)
        assert.equal(theOrder, '6000.00')
        assert.equal(orderUuid.length, 8)
    })
  })
})

describe('Search', async () => {
  describe('Search for body bottom wood: Maple', async () => {
    it('should return two models', async () => {
        let query = [
          {
              featureId: 2,
              specIds: [2]
          }
        ]
        const modelIds = await findModelIds(query)
        assert.equal(modelIds.find(m => m === 6), 6)
        assert.equal(modelIds.find(m => m === 8), 8)
    })
  })
})
