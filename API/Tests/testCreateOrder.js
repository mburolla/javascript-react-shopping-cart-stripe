
const order = require('../Checkout/Order')
const mySqlProxy = require('../Proxies/MySqlProxy')

// eslint-disable-next-line no-unused-vars
const createOrder = async () => {
    const items = [
        {
            modelId: 5, 
            qty: 2
        },
        {
            modelId: 1, 
            qty: 1
        }
      ]
      const shipping = {
          name: "Martin J. Burolla",
          address: {
            line1: "123 Green Street",
            line2: "",
            city: "Ontario",
            state: "NY",
            zip: "14519-9343"
          }
      }
      const orderUuid = await order.createOrder(items, shipping)
      const theOrder = await mySqlProxy.getOrder(orderUuid)
      console.log(theOrder)
      process.exit()
}

// eslint-disable-next-line no-unused-vars
const getOrderItems = async () => {
    const orderItems = await order.getOrderItems('B7402D49')
    console.log(orderItems)
    process.exit()
}

const main = async () => {
    await createOrder()
    //await getOrderItems()
}

main()



