/* eslint-disable no-unused-vars */

const mySqlProxy = require('../Proxies/MySqlProxy')

const findModelIds = async (query) => {
    // Let the database OR the specs for a feature...
    let featureDataSetArray = []
    for (const q of query) {
        featureDataSetArray.push(await mySqlProxy.getProductsForSpecIds(q.featureId, q.specIds)) 
    }

    // Let JavaScript AND each feature group...
    let index = 0
    let intersect = null
    if (featureDataSetArray.length >= 2) {
        while (index < featureDataSetArray.length - 1) {
            if (index === 0) {
                const s1 = new Set(featureDataSetArray[index].map(i => i.model_id))
                const s2 = new Set(featureDataSetArray[++index].map(i => i.model_id))
                intersect = new Set([...s1].filter(i => s2.has(i))) // Intersect
            }
            else {
                ++index
                const s1 = new Set(featureDataSetArray[index].map(i => i.model_id))
                const s2 = intersect
                intersect = new Set([...s1].filter(i => s2.has(i))) // Intersect
                ++index
            }
        }
    }
    else {
        intersect = new Set(featureDataSetArray[0].map(i => i.model_id)) // remove dupes
    }
    return Array.from(intersect)
}

const main = async () => {

    let query1 = [
        {
            featureId: 6,
            specIds: [21, 22]
        }
    ]
    let query2 = [
        {
            featureId: 6,
            specIds: [21, 22]
        },
        {
            featureId: 2,
            specIds: [2]
        }
    ]
    let query3 = [
        {
            featureId: 6,
            specIds: [21, 22]
        },
        {
            featureId: 2,
            specIds: [2]
        },
        {
            featureId: 3,
            specIds: [7]
        },
    ]
    const modelIds = await findModelIds(query2)  // Make a call to the db for these model ids.
    console.log(modelIds)
    process.exit()
}

main()
