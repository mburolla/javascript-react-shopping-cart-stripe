//
// File: server.js
// Auth: Martin Burolla
// Date: 10/24/2022
// Desc: Express Web API for the shopping cart.
//

const cors = require('cors')
const express = require('express')
const app = express()
const common = require('./Common')
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
//
const logRouter = require('./Routes/Log')
const healthRouter = require('./Routes/Health')
const ordersRouter = require('./Routes/Orders')
const modelsRouter = require('./Routes/Models')
const searchRouter = require('./Routes/Search')
const featuresRouter = require('./Routes/Features')
const paymentIntentsRouter = require('./Routes/PaymentIntents')
const productCatalogRouter = require('./Routes/ProductCatalog')

//
const helmet = require('helmet')
const jwksRsa = require('jwks-rsa')
const { expressjwt: jwt } = require('express-jwt')
//
const auth0audience = `eCommerce-api` // identifier
const auth0Domain = `dev-t2ndw215zcxbp5x6.us.auth0.com`

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Store API',
      version: '0.1.0',
      description: 'Express API for store front', 
      servers: ['http://localhost:5150']
    }
  },
  apis: [
    './API/server.js', 
    './API/Routes/Log.js',
    './API/Routes/Orders.js',
    './API/Routes/Health.js',
    './API/Routes/Models.js',
    './API/Routes/Search.js',
    './API/Routes/Features.js',
    './API/Routes/PaymentIntents.js',
    './API/Routes/ProductCatalog.js',
  ]
}

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${auth0Domain}/.well-known/jwks.json`
  }),
  audience: auth0audience,
  issuer: `https://${auth0Domain}/`,
  algorithms: ['RS256']
})

const swaggerDocs = swaggerJsDoc(swaggerOptions)

app.use(helmet())
app.use('/orders', checkJwt)          // private
app.use('/payment-intents', checkJwt) // private
app.use('/product-catalogs', checkJwt)// private
app.use(cors(common.corsOptions))
app.use(express.json())
app.use(common.requestConsoleLogger)
app.use(express.static('public'))
app.use('/', logRouter)
app.use('/', healthRouter)
app.use('/', ordersRouter)
app.use('/', modelsRouter)
app.use('/', searchRouter)
app.use('/', featuresRouter)
app.use('/', paymentIntentsRouter)
app.use('/product-catalogs', productCatalogRouter)
app.use('/api-docs/', swaggerUi.serve, swaggerUi.setup(swaggerDocs))

app.listen(5150, () => console.log('Express API running on port 5150.'))
