exports.connString = {
    user: 'xxx',      // <==== Update with your db user name.
    password: 'xxx',  // <==== Update with password for this user.
    host: '127.0.0.1',
    database: 'guitar-store',
    multipleStatements: true
}
