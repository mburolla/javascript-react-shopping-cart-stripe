//
// File: Exceptions.js
// Auth: Martin Burolla
// Date: 11/3/2022
// Desc: Contains all the custom exceptions for the API.
//       An error that is thrown that is not one of these 
//       classes returns an HTTP status code of 500.
//

const handleError = (res, error) => {
    let retval = null;
    switch(error.constructor) {
        case ValidationError:
            retval = res.status(400).json({ errors: error.message})
            break;
        case BadRequestError:
            retval = res.status(400).json({ error: error.message})
            break;
        case NotFoundError:
            retval = res.status(404).json({ error: error.message})
            break;
        default:
            retval = res.status(500).json({ error: error.message})
    }
    return retval
}

class ValidationError extends Error { // 400
    constructor(message = "", ...args) {
      super(message, ...args);
      this.message = message;
    }
}

class BadRequestError extends Error { // 400
    constructor(message = "", ...args) {
      super(message, ...args);
      this.message = message;
    }
}

class NotFoundError extends Error { // 404
    constructor(message = "", ...args) {
      super(message, ...args);
      this.message = message;
    }
}

module.exports.handleError = handleError
module.exports.NotFoundError = NotFoundError
module.exports.BadRequestError = BadRequestError
module.exports.ValidationError = ValidationError
