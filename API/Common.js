//
// File: Common.js
// Auth: Martin Burolla
// Date: 10/31/2022
// Desc: Common things shared among all routes.
//

const { 
    readWindowMsRateLimit, 
    readMaxRateLimit,
    writeWindowMsRateLimit,
    writeMaxRateLimit
} = require('./App.config.js')
const jwt_decode = require('jwt-decode')
const logging = require('./Logging/Logging')
const rateLimit = require('express-rate-limit')
const { validationResult } = require('express-validator')
const { ValidationError } = require('./Exceptions.js')

exports.requestCount = 0

exports.corsOptions = {
    origin: 'http://localhost:3000',  // <== do not use array for only one value here.
    optionsSuccessStatus: 200
}

exports.readLimit = rateLimit({
	windowMs: readWindowMsRateLimit,
	max: readMaxRateLimit, 
	standardHeaders: true,
	legacyHeaders: false,
})

exports.writeLimit = rateLimit({
	windowMs: writeWindowMsRateLimit,
	max: writeMaxRateLimit,
	standardHeaders: true,
	legacyHeaders: false,
})

exports.requestDBLogger = async (req, res, next) => {
    let orderUuid = req.body?.orderUuid
    if (!orderUuid) {
      orderUuid = req.params.orderUuid
    }
    if (!orderUuid) {
      orderUuid = 'NA'
    }
    const message = `${this.requestCount} ${req.method} ${req.originalUrl}`
    await logging.insertStripeLog(orderUuid, req.socket.remoteAddress, message)
    next();
}

exports.requestConsoleLogger = (req, res, next) => {
    const message = `${++this.requestCount} ${req.socket.remoteAddress} ${req.method} ${req.originalUrl}`
    console.log(message)
    next();
}

exports.validateRequest = (req) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      throw new ValidationError(errors.array())
  }
}

exports.getUserIdFromToken = (req) => {
  const decodedToken = jwt_decode(req.headers.authorization)
  return decodedToken.sub
}
