// 
// File: Logging.js
// Auth: Martin Burolla
// Date: 10/30/2022
// Desc: Just logging nothing to see here.
//

const mySqlProxy = require('../Proxies/MySqlProxy')

exports.insertStripeLog = async (orderUuid, ip, message) => {
    try {
        await mySqlProxy.insertStripeLog(orderUuid, ip, message)
    }
    catch(error) {
        // Who cares about logging?
    }
}
