/**
 * Version: seed
 */

/**
 * Schema
 */

CREATE TABLE `stripe_log` (
  `stripe_log_id` int NOT NULL AUTO_INCREMENT,
  `order_uuid` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `message` varchar(128) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stripe_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `model` (
  `model_id` int NOT NULL AUTO_INCREMENT,
  `model_number` varchar(11) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `product_catalog` (
  `product_catalog_id` int NOT NULL AUTO_INCREMENT,
  `catalog_name` varchar(45) NOT NULL,
  `is_active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`product_catalog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `order_status` (
  `order_status_id` int NOT NULL AUTO_INCREMENT,
  `the_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`order_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_catalog_id` int NOT NULL,
  `product_desc` varchar(45) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `make` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`product_id`),
  KEY `product_id` (`product_id`),
  KEY `fk_product_catalog_id_idx` (`product_catalog_id`),
  CONSTRAINT `fk_product_catalog_id` FOREIGN KEY (`product_catalog_id`) REFERENCES `product_catalog` (`product_catalog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `product_order` (
  `product_order_id` int NOT NULL AUTO_INCREMENT,
  `order_status_id` int NOT NULL DEFAULT '1',
  `order_uuid` varchar(12) NOT NULL,
  `user_id` varchar(128) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `shipping_name` varchar(45) NOT NULL,
  `shipping_address_1` varchar(45) DEFAULT NULL,
  `shipping_address_2` varchar(45) NOT NULL,
  `shipping_city` varchar(45) NOT NULL,
  `shipping_state` varchar(49) NOT NULL,
  `shipping_zip` varchar(13) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stripe_payment_intent_id` varchar(45) DEFAULT NULL,
  `stripe_payment_intent_status` varchar(45) DEFAULT NULL,
  `shipping_tracking_info` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`product_order_id`),
  KEY `fk_product_order_idx` (`order_status_id`),
  CONSTRAINT `fk_product_order_2` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`order_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `order_item` (
  `order_item_id` int NOT NULL AUTO_INCREMENT,
  `product_order_id` int NOT NULL,
  `model_id` int NOT NULL,
  `make` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `purchase_price` decimal(10,2) NOT NULL,
  `total_spec_price` decimal(10,2) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `qty` int NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_item_id`),
  KEY `fk_product_order_idx` (`product_order_id`),
  KEY `fk_config_id_3_idx` (`model_id`),
  CONSTRAINT `fk_config_id_3` FOREIGN KEY (`model_id`) REFERENCES `model` (`model_id`),
  CONSTRAINT `fk_product_order` FOREIGN KEY (`product_order_id`) REFERENCES `product_order` (`product_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `feature` (
  `feature_id` int NOT NULL AUTO_INCREMENT,
  `feature_desc` varchar(45) DEFAULT NULL,
  `feature_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `specification` (
  `specification_id` int NOT NULL AUTO_INCREMENT,
  `feature_id` int NOT NULL,
  `spec_desc` varchar(45) DEFAULT NULL,
  `spec_value` varchar(45) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`specification_id`),
  KEY `fk_feature_2_idx` (`feature_id`),
  CONSTRAINT `fk_feature_2` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `product_feature_spec` (
  `product_feature_spec_id` int NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `feature_id` int NOT NULL,
  `specification_id` int NOT NULL,
  PRIMARY KEY (`product_feature_spec_id`),
  KEY `fk_product_id_3_idx` (`product_id`),
  KEY `fk_feature_id_1_idx` (`feature_id`),
  KEY `fk_spec_1_idx` (`specification_id`),
  CONSTRAINT `fk_feature_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`feature_id`),
  CONSTRAINT `fk_product_id_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `fk_spec_1` FOREIGN KEY (`specification_id`) REFERENCES `specification` (`specification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `purchased_specification` (
  `purchased_specification_id` int NOT NULL AUTO_INCREMENT,
  `order_item_id` int NOT NULL,
  `specification_id` int NOT NULL,
  `purchased_price` decimal(10,2) DEFAULT '0.00',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchased_specification_id`),
  KEY `fk_spec_2_idx` (`specification_id`),
  KEY `fk_order_item_idx` (`order_item_id`),
  CONSTRAINT `fk_order_item` FOREIGN KEY (`order_item_id`) REFERENCES `order_item` (`order_item_id`),
  CONSTRAINT `fk_spec_2` FOREIGN KEY (`specification_id`) REFERENCES `specification` (`specification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `available_product_feature_spec` (
  `available_product_feature_spec_id` int NOT NULL AUTO_INCREMENT,
  `product_feature_spec_id` int NOT NULL,
  `model_id` int NOT NULL,
  `showcase_order` int DEFAULT '0',
  PRIMARY KEY (`available_product_feature_spec_id`),
  KEY `fk_prod_feature_spec_id_idx` (`product_feature_spec_id`),
  KEY `idx_config_id` (`model_id`),
  CONSTRAINT `fk_config_id_2` FOREIGN KEY (`model_id`) REFERENCES `model` (`model_id`),
  CONSTRAINT `fk_prod_feature_spec_id` FOREIGN KEY (`product_feature_spec_id`) REFERENCES `product_feature_spec` (`product_feature_spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `inventory` (
  `inventory_id` int NOT NULL AUTO_INCREMENT,
  `model_id` int NOT NULL,
  `qty` int NOT NULL,
  PRIMARY KEY (`inventory_id`),
  KEY `fk_config_id_idx` (`model_id`),
  CONSTRAINT `fk_config_id` FOREIGN KEY (`model_id`) REFERENCES `available_product_feature_spec` (`available_product_feature_spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


/**
 * Data
 */

insert into order_status (the_status) values ('New');
insert into order_status (the_status) values ('Building');
insert into order_status (the_status) values ('Built');
insert into order_status (the_status) values ('Shipping');
insert into order_status (the_status) values ('Shipped');
insert into order_status (the_status) values ('Full Refund');
insert into order_status (the_status) values ('Partial Refund');

insert into product_catalog (catalog_name, is_active) values ('Guitars', 1);
insert into product_catalog (catalog_name, is_active) values ('Bass', 1);

insert into product (product_catalog_id, product_desc, price, make, model, active) 
values (1, 'Classic guitar sounds', 1000.00, 'Fender', 'Strat', 1);

insert into product (product_catalog_id, product_desc, price, make, model, active) 
values (1, 'Ultra modern guitar', 2000.00, 'Suhr', 'Modern', 1);

insert into product (product_catalog_id, product_desc, price, make, model, active) 
values (1, 'Classic fat tones', 3000.00, 'Gibson', 'Les Paul', 1);

insert into feature (feature_desc) values ('BASE');             -- 1
insert into feature (feature_desc) values ('Body bottom wood'); -- 2
insert into feature (feature_desc) values ('Body top wood');    -- 3
insert into feature (feature_desc) values ('Neck bottom wood'); -- 4
insert into feature (feature_desc) values ('Neck top wood');    -- 5
insert into feature (feature_desc) values ('Frets');            -- 6
insert into feature (feature_desc) values ('Tuners');           -- 7
insert into feature (feature_desc) values ('Body finish');      -- 8
insert into feature (feature_desc) values ('Neck finish');      -- 9
insert into feature (feature_desc) values ('Bridge');           -- 10
insert into feature (feature_desc) values ('Neck Radius');      -- 11
insert into feature (feature_desc) values ('Nut');              -- 12

insert into specification (feature_id, spec_desc) values (2, 'BASE');
insert into specification (feature_id, spec_desc) values (2, 'Maple'); 
insert into specification (feature_id, spec_desc) values (2, 'Adler'); 
insert into specification (feature_id, spec_desc) values (2, 'Mahagony'); 
insert into specification (feature_id, spec_desc) values (2, 'Basswood'); 
insert into specification (feature_id, spec_desc) values (2, 'Swamp Ash'); 
insert into specification (feature_id, spec_desc) values (3, 'Maple'); 
insert into specification (feature_id, spec_desc) values (3, 'Adler'); 
insert into specification (feature_id, spec_desc) values (3, 'Mahagony'); 
insert into specification (feature_id, spec_desc) values (3, 'Basswood'); 
insert into specification (feature_id, spec_desc) values (3, 'Swamp Ash'); 
insert into specification (feature_id, spec_desc) values (4, 'Maple'); 
insert into specification (feature_id, spec_desc) values (4, 'Adler'); 
insert into specification (feature_id, spec_desc) values (4, 'Mahagony'); 
insert into specification (feature_id, spec_desc) values (4, 'Basswood'); 
insert into specification (feature_id, spec_desc) values (4, 'Swamp Ash');
insert into specification (feature_id, spec_desc) values (5, 'Maple'); 
insert into specification (feature_id, spec_desc) values (5, 'Rosewood'); 
insert into specification (feature_id, spec_desc) values (5, 'Pau Ferro'); 
insert into specification (feature_id, spec_desc) values (5, 'Ebony'); 
insert into specification (feature_id, spec_desc) values (6, 'Nickel Alloy'); 
insert into specification (feature_id, spec_desc, cost) values (6, 'Stainless steel frets', 100); 
insert into specification (feature_id, spec_desc, cost) values (6, 'Stainless steel - Jumbo', 100); 
insert into specification (feature_id, spec_desc) values (7, 'Non locking'); 
insert into specification (feature_id, spec_desc) values (7, 'Locking'); 
insert into specification (feature_id, spec_desc) values (8, 'Glossy'); 
insert into specification (feature_id, spec_desc) values (8, 'Satin'); 
insert into specification (feature_id, spec_desc) values (8, 'Nitro'); 
insert into specification (feature_id, spec_desc) values (9, 'Glossy'); 
insert into specification (feature_id, spec_desc) values (9, 'Satin'); 
insert into specification (feature_id, spec_desc) values (9, 'Nitro'); 
insert into specification (feature_id, spec_desc) values (10, 'Hard tail bridge'); 
insert into specification (feature_id, spec_desc, cost) values (10, 'Floyd Rose tremolo', 100.00); 
insert into specification (feature_id, spec_desc) values (10, 'Gotoh 510 tremolo'); 
insert into specification (feature_id, spec_desc) values (10, 'Hipshot'); 
insert into specification (feature_id, spec_desc, spec_value) values (11, 'Compound', '10"-14"'); 
insert into specification (feature_id, spec_desc, spec_value) values (11, 'Compound', '12"-16"'); 
insert into specification (feature_id, spec_desc, spec_value) values (11, 'Linear', '16"'); 
insert into specification (feature_id, spec_desc, spec_value) values (11, 'Linear', '12"'); 
insert into specification (feature_id, spec_desc) values (12, 'Bone'); 
insert into specification (feature_id, spec_desc) values (12, 'Tusq'); 
insert into specification (feature_id, spec_desc) values (12, 'Plastic'); 

-- Our base "no options" products
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 1, 1);
insert into product_feature_spec (product_id, feature_id, specification_id) values (2, 1, 1);
insert into product_feature_spec (product_id, feature_id, specification_id) values (3, 1, 1);

-- Fender Strat
-- All the POSSIBLE options for a Fender Strat.
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 2, 2);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 2, 3);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 2, 4);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 3, 5);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 3, 6);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 3, 7);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 4, 12);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 5, 17);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 5, 18);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 6, 21);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 6, 22);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 7, 24);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 7, 25);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 8, 26);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 9, 30);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 10, 32);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 10, 33);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 11, 39);
insert into product_feature_spec (product_id, feature_id, specification_id) values (1, 12, 42);

-- Add our product configuration SKUs...
insert into model (model_number, is_active) values ('00000000001', 1); -- 1
insert into model (model_number, is_active) values ('00000000002', 1); -- 2
insert into model (model_number, is_active) values ('00000000003', 1); -- 3
insert into model (model_number, is_active) values ('00000000004', 1); -- 4
insert into model (model_number, is_active) values ('00000000005', 1); -- 5
--
insert into model (model_number, is_active) values ('00000000006', 1); -- 6
insert into model (model_number, is_active) values ('00000000007', 1); -- 7
insert into model (model_number, is_active) values ('00000000008', 1); -- 8
insert into model (model_number, is_active) values ('00000000009', 1); -- 9

-- Add our base products configurations...
insert into available_product_feature_spec (product_feature_spec_id, model_id, showcase_order) values (1, 1, 1);  -- Base Fender Strat
insert into available_product_feature_spec (product_feature_spec_id, model_id, showcase_order) values (2, 2, 2);  -- Base Suhr Modern
insert into available_product_feature_spec (product_feature_spec_id, model_id, showcase_order) values (3, 3, 3);  -- Base Gibson LP
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (20, 4); -- Fender Strat Floyd Rose
                                                                                               -- Fender Strat:
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (14, 5); --   Stainless steel frets 
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (20, 5); --   Floyd Rose
--
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (4, 6);  -- Maple bottom
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (9, 6);  -- Maple top
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (14, 6); -- Stainless steel frets
--
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (5, 7);  -- Alder bottom
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (9, 7);  -- Maple top
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (13, 7); -- Nickel Alloy frets
--
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (4, 8);  -- Maple bottom
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (9, 8);  -- Maple top
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (13, 8); -- Nickel Alloy frets
--
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (6, 9);  -- Mahagony bottom
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (9, 9);  -- Maple top
insert into available_product_feature_spec (product_feature_spec_id, model_id) values (13, 9); -- Nickel Alloy frets


-- Add the quantity of our product configurations...
insert into inventory (model_id, qty) values (1, 10); -- Base Fender
insert into inventory (model_id, qty) values (2, 20); -- Base Suhr
insert into inventory (model_id, qty) values (3, 30); -- Base Gibson
insert into inventory (model_id, qty) values (4, 1);  -- Custom Fender Floyd Rose
insert into inventory (model_id, qty) values (5, 1);  -- Custom Fender stainless steel
--
insert into inventory (model_id, qty) values (6, 1); 
insert into inventory (model_id, qty) values (7, 1); 
insert into inventory (model_id, qty) values (8, 1);  
insert into inventory (model_id, qty) values (9, 1);

/**
 * Views
 */

CREATE VIEW `view_product_feature_spec` AS
select 
    pfs.product_feature_spec_id,
    pfs.product_id,
    pfs.feature_id,
    pfs.specification_id,
    f.feature_desc,
    s.spec_desc,
    s.spec_value
from 
    product_feature_spec pfs
    join feature f on pfs.feature_id = f.feature_id
    join specification s on pfs.specification_id = s.specification_id;

CREATE VIEW `view_available_models` AS
select 
    p.product_id, 
    pfs.feature_id,
    pfs.specification_id,
    m.model_id,
    m.model_number,
    p.make,
    p.model,
    p.product_desc,
    p.price,
    f.feature_desc,
    s.spec_desc,
    s.spec_value,
    s.cost,
    p.active,
    i.qty,
    a.showcase_order
  from 
    inventory i 
    join available_product_feature_spec a on i.model_id = a.model_id
    join product_feature_spec pfs on pfs.product_feature_spec_id = a.product_feature_spec_id
    join feature f on f.feature_id = pfs.feature_id
    join specification s on pfs.specification_id = s.specification_id
    join product p on p.product_id = pfs.product_id
    join model m on m.model_id = a.model_id;

/*
 
DELIMITER $$
CREATE PROCEDURE `reset_data`()
BEGIN
	delete from purchased_specification;
	delete from order_item;
  delete from product_order;
	delete from stripe_log;
END$$
DELIMITER ;

*/
