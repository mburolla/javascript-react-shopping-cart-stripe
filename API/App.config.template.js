exports.stripeKey = 'sk_test_xxx' // <=== Update with secret key from Stripe.
exports.readWindowMsRateLimit = 1000 * 10 // 10 seconds
exports.readMaxRateLimit = 30
exports.writeWindowMsRateLimit = 1000 * 10 // 10 seconds
exports.writeMaxRateLimit = 5
