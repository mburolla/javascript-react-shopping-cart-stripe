//
// File: Search.js
// Auth: Martin Burolla
// Date: 11/13/2022
// Desc: All the business logic associated with Search.
//

const mySqlProxy = require('../Proxies/MySqlProxy')

exports.findModelIds = async (query) => {
    // Let the database OR the specs for a feature...
    let featureDataSetArray = []
    for (const q of query) {
        featureDataSetArray.push(await mySqlProxy.getProductsForSpecIds(q.featureId, q.specIds)) 
    }

    // Let JavaScript AND each feature group...
    let index = 0
    let intersect = null
    if (featureDataSetArray.length >= 2) {
        while (index < featureDataSetArray.length - 1) {
            if (index === 0) {
                const s1 = new Set(featureDataSetArray[index].map(i => i.model_id))
                const s2 = new Set(featureDataSetArray[++index].map(i => i.model_id))
                intersect = new Set([...s1].filter(i => s2.has(i))) // Intersect
            }
            else {
                ++index
                const s1 = new Set(featureDataSetArray[index].map(i => i.model_id))
                const s2 = intersect
                intersect = new Set([...s1].filter(i => s2.has(i))) // Intersect
                ++index
            }
        }
    }
    else {
        intersect = new Set(featureDataSetArray[0].map(i => i.model_id)) // remove dupes
    }
    return Array.from(intersect)
}
