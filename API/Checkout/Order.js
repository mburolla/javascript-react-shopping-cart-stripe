//
// File: Order.js
// Auth: Martin Burolla
// Date: 10/26/2022
// Desc: The business logic that creates an Order, order items,
//       purchased specs and determines the total dollar amount 
//       for the order.
//

const mySqlProxy = require('../Proxies/MySqlProxy')
const { BadRequestError } = require('../Exceptions')

exports.createOrder = async (items, shipping, userId) => {
    const models = await mySqlProxy.getProducts() // available models
    validateItems(items, models) // throws

    const conn = await mySqlProxy.promisePool.getConnection()
    
    try {
        await conn.beginTransaction()

        // Insert into parent table (product_order).
        const { orderUuid, productOrderId } = await mySqlProxy.insertOrder(conn, shipping, userId)

        // Insert into child table (order_items).
        for (const i of items) { 
            const specs = models.filter(p => p.model_id === i.modelId)
            const totalSpecPrice = (specs.map(i => Number(i.cost))).reduce((a,b) => a + b, 0)
            const totalPrice = (totalSpecPrice + Number(specs[0].price)) * i.qty
            const orderItemId = await mySqlProxy.insertOrderItem(conn, 
                productOrderId, 
                i.modelId, 
                i.qty, 
                specs[0].price, 
                specs[0].make, 
                specs[0].model,
                totalSpecPrice,
                totalPrice
              )
            
            // Insert into child table (purchased_specification).
            for (const spec of specs) {
              await mySqlProxy.insertPurchasedSpec(conn, orderItemId, spec.specification_id, spec.cost)
            }
        }

        // Calculate the total order amount and update product_order.amount.
        const orderTotal = await mySqlProxy.getTotalForOrder(conn, productOrderId)
        await mySqlProxy.updateProductOrderPrice(conn, productOrderId, orderTotal)

        // throw new Error('transaction test')
        await conn.commit()
        return orderUuid
    }
    catch(error) {
        throw(error)
    }
    finally {
        await conn.release()
    }
}

const validateItems = (items, models) => {
  for (const i of items) { 
    const product = models.find(p => p.model_id === i.modelId)
    if (!product?.price) {
      throw new BadRequestError(`Invalid modelId: ${i.modelId}.`)
    }
    if (!i.qty) {
      throw new BadRequestError(`Missing quantity.`)
    }
  }
}
