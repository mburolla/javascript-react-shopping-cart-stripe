//
// File: Currency.js
// Auth: Martin Burolla
// Date: 10/27/2022
// Desc: All money functions.
//

exports.formatUSD = (money) => {
    const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' })
    return formatter.format(money)
}

exports.formatUSDFromStripe = (money) => {
    money = String(money)
    const left = money.substring(0,money.length-2)
    const right = money.substring(money.length-2)
    money = `${left}.${right}`
    const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' })
    return formatter.format(money)
}
