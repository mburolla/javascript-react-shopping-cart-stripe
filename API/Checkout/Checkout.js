//
// File: Checkout.js
// Auth: Martin Burolla
// Date: 10/30/2022
// Desc: All the logic responsible for showing and selling products.
//

const order = require('./Order')
const { findModelIds } = require('./Search')
const { NotFoundError } = require('../Exceptions')
const mySqlProxy = require('../Proxies/MySqlProxy')
const { formatUSDFromStripe } = require('./Currency')
const { retrievePaymentIntent, createPaymentIntent } = require('../Proxies/StripeProxy')

//
// Public
//

exports.findModels = async (query) => {
    const modelIds = await findModelIds(query.query)
    const models = await mySqlProxy.getModelsForSearch(modelIds)
    return convertModelRowsToObjects(models)
}

exports.getFeaturesAndSpecs = async () => {
    const featureRows = await mySqlProxy.getFeaturesAndSpecs()
    return convertFeatureRowsToObjects(featureRows)
}

exports.getModels = async () => {
    let models = null
    models = await mySqlProxy.getProducts()
    return convertModelRowsToObjects(models)
}

exports.search = async (specIds) => {
    let products = await mySqlProxy.getProducts()
    return convertModelRowsToObjects(products)
}

exports.getOrderAmount = async (orderUuid) => {
    const retval = await mySqlProxy.getOrderAmount(orderUuid)
    if (!retval) {
        throw new NotFoundError("OrderUuid not found.")
    }
    return retval
}

exports.getShippingInforForOrder = async (orderUuid) => {
    const retval = await mySqlProxy.getShippingInfo(orderUuid)
    if (!retval) {
        throw new NotFoundError("OrderUuid not found.")
    }
    return retval
}

exports.getItemsForOrder = async (orderUuid) => {
    let retval = {}
    retval.results = await mySqlProxy.getOrderItems(orderUuid)
    if (retval.results.length === 0) {
        throw new NotFoundError("OrderUuid not found.")
    }
    return retval.results
}
  
exports.createOrder = async (items, shipping, userId) => {
    let retval = {}
    retval.results = await order.createOrder(items, shipping, userId)
    return retval.results
}

exports.createPaymentIntent = async (orderUuid) => {
    let retval = {}
    const totalAmount = await mySqlProxy.getPriceForOrder(orderUuid)
    if (!totalAmount) {
        throw new NotFoundError("OrderUuid not found.")
    }
    const paymentIntent = await createPaymentIntent(orderUuid, totalAmount)
    await mySqlProxy.updateOrderWithPaymentIntent(paymentIntent.id, orderUuid)
    retval.results = paymentIntent.client_secret
    return retval.results
}

exports.updateOrderStatus = async (orderUuid, paymentIntentId) => { // paymentIntentId comes from the Stripe URL redirect.
    let retval = {}
    const order = await mySqlProxy.getOrderWithPaymentIntent(orderUuid, paymentIntentId)
    if (!order) {
        throw new NotFoundError("Order not found.")
    }
    const paymentIntent = await retrievePaymentIntent(order.pid) // Payment Intent from Stripe API.
    const dollarAmount = formatUSDFromStripe(paymentIntent.amount_received)
    const paymentStatus = `${paymentIntent.status}: ${dollarAmount}`
    await mySqlProxy.updateOrderWithPaymentStatus(orderUuid, paymentIntentId, paymentStatus) 
    retval.results = paymentStatus
    return retval.results
}

//
// Private
//

const convertFeatureRowsToObjects = (features) => {
    let retval = []
    let featureId = -1
    features.forEach(f => {
        if (featureId !== f.feature_id) {
            featureId = f.feature_id
            retval.push({
                featureId: f.feature_id,
                featureDesc: f.feature_desc,
                specs: [{
                    specificationId: f.specification_id,
                    description: f.spec_desc,
                    value: f.spec_value ?? 0,
                    cost: f.cost ?? 0
                }]
            })
        }
        else {
            const endFeature = retval[retval.length - 1]
            endFeature.specs.push({ 
                specificationId: f.specification_id,
                description: f.spec_desc,
                value: f.spec_value ?? 0,
                cost: f.cost ?? 0
            })
        }
    })
    return retval
}

const convertModelRowsToObjects = (models) => {
    let retval = []
    let modelId = -1
    models.forEach(p => {
        if (modelId !== p.model_id) {
            modelId = p.model_id
            retval.push( {
                model_id: p.model_id,
                product_id: p.product_id,
                modelNumber: p.model_number,
                make: p.make,
                model: p.model,
                product_desc: p.product_desc,
                base_price: p.price,
                price: Number(p.price) + Number(p.cost),
                showcase_order: p.showcase_order,
                specs : [
                    { 
                        feature_id: p.feature_id,
                        feature_desc: p.feature_desc,
                        specification_id: p.specification_id,
                        spec_desc: p.spec_desc,
                        cost: p.cost ?? 0.00
                    }
                ]
            })
        } else {
            const endModel = retval[retval.length - 1]
            endModel.specs.push(
                { 
                    feature_id: p.feature_id,
                    feature_desc: p.feature_desc,
                    specification_id: p.specification_id,
                    spec_desc: p.spec_desc,
                    cost: p.cost ?? 0.00
                }
            )
            endModel.price += Number(p.cost)
        } 
    })
    return retval
}
