//
// File: ProductCatalogManager.js
// Auth: Martin Burolla
// Date: 12/19/2022
// Desc: Contains all the business logic for the Product Catalog.
//      

const mySqlProxy = require('../Proxies/MySqlProxy')

exports.getProductDetails = async (productCatalogId, productId) => {
    const product = await mySqlProxy.getProductDetails(productCatalogId, productId)
    const featureSpecs = product.map(p => {
        return {
            featureId: p.feature_id,
            specificationId: p.specification_id,
            desc: p.feature_desc,
            value: p.feature_value,
            specDesc: p.spec_desc,
            specValue: p.spec_value,
            cost: p.cost
        }
    })
    const summary = {
        productId: product[0].product_id,
        description: product[0].product_desc,
        price: product[0].price,
        make: product[0].make,
        model: product[0].model
    }
    return { summary, featureSpecs}
}

exports.getProducts = async (productCatalogId) => {
    const  products = await mySqlProxy.getProductsAdmin(productCatalogId)
    return products
}

exports.getProductCatalogs = async () => {
    const  productCatalogs = await mySqlProxy.getProductCatalog()
    return productCatalogs
}

exports.getFeatures = async () => {
    const  features = await mySqlProxy.getFeatures()
    return features
}

exports.getSpecificationsForFeature = async (featureId) => {
    const  specifications = await mySqlProxy.getSpecifications(featureId)
    return specifications
}

exports.addProduct = async (product) => {
    const productId = await mySqlProxy.addProduct(product)
    return productId
}

exports.associateSpecWithFeature = async (productId, specId, featureId) => {
    const productFeatureSpecId = await mySqlProxy.addSpecForFeature(productId, specId, featureId)
    return productFeatureSpecId
}

exports.addFeature = async (description) => {
    const productId = await mySqlProxy.addFeature(description)
    return productId
}

exports.addSpecification = async (featureId, description, specValue, cost) => {
    const productId = await mySqlProxy.addSpecification(featureId, description, specValue, cost)
    return productId
}
