//
// File: Search.js
// Auth: Martin Burolla
// Date: 11/10/2022
// Desc: All the routes for /search
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const { findModels } = require('../Checkout/Checkout')
const { handleError } = require('../Exceptions')
const { check } = require('express-validator')

/**
 * @swagger
 * /search:
 *   post: 
 *     tags:
 *       - Shopping
 *     description: Returns all the models for the specified query.
 *     consumes:
 *        - application/json
 *     parameters:
 *        - in: body
 *          name: Query
 *          description: The models to search for
 *          schema:
 *            type: object
 *            properties:
 *              query:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    featureId:
 *                      type: integer
 *                      example: 3
 *                    specIds: 
 *                      type: Array
 *                      example: [7]
 *     responses:
 *       '200':
 *          description: Models.
 */ 
 router.post(
    '/search', 
    common.readLimit, 
    common.requestDBLogger,
    check('items').not().isEmpty(),
    async (req, res) => {
        try {
            const query = req.body
            const results = await findModels(query)
            res.send({ products: results })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

module.exports = router
