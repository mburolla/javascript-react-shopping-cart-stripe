//
// File: Log.js
// Auth: Martin Burolla
// Date: 10/31/2022
// Desc: All the routes for /Log
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const logging = require('../Logging/Logging')

/**
 * @swagger
 * /log:
 *    post:
 *     tags:
 *       - Utility
 *     description: Adds a log entry.
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: orderUuid
 *         description: The order id to update
 *         schema:
 *           type: object
 *           required:
 *             - orderUuid
 *             - message
 *           properties:
 *             orderUuid:
 *               type: string
 *               example: CF49F6B7
 *             ip:
 *               type: string
 *               example: '13.252.6.252'
 *             message:
 *               type: string
 *               example: 'Swagger API: Created order'
 *     responses:
 *       '200':
 *          description: Order status.
 */
router.post(
    '/log', 
    common.writeLimit,
    async (req, res) => {
      const { orderUuid, ip, message } = req.body
      await logging.insertStripeLog(orderUuid, ip, message)
      res.send({ status: "ok" })
    }
)

module.exports = router
