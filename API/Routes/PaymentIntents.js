//
// File: PaymentIntents.js
// Auth: Martin Burolla
// Date: 10/31/2022
// Desc: All the routes for /payment-intents
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const { body } = require('express-validator')
const checkout = require('../Checkout/Checkout')
const { handleError } = require('../Exceptions')

/**
 * @swagger
 * /payment-intents:
 *    post:
 *      tags:
 *        - Checkout
 *      description: 'Creates a Stripe Payment Intent for an order. The Payment Intent
 *         contains the total price of the order.  The OrderUuid is stored as metadata
 *         in Stripe.'
 *      consumes:
 *        - application/json
 *      parameters:
 *        - in: body
 *          name: Order Uuid
 *          description: Order id
 *          schema:
 *            type: object
 *            required:
 *              - orderUuid
 *            properties:
 *              orderUuid:
 *                type: string
 *                example: CF49F6B7
 *      responses:
 *        '200':
 *           description: 'Client secret: pi_xxx_secret_xxx'
 *        '400':
 *          description: Validation errors.
 */
 router.post(
    '/payment-intents', 
    common.writeLimit, 
    common.requestDBLogger,
    body('orderUuid').isLength({min: 8}),  
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const { orderUuid } = req.body
            const results = await checkout.createPaymentIntent(orderUuid)
            res.send({ clientSecret: results })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)
  
module.exports = router
