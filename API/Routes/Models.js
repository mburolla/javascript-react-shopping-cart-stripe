//
// File: Products.js
// Auth: Martin Burolla
// Date: 10/31/2022
// Desc: All the routes for /models
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const checkout = require('../Checkout/Checkout')
const { handleError } = require('../Exceptions')

/**
 * @swagger
 * /models:
 *   get: 
 *     tags:
 *       - Shopping
 *     description: Returns all the active models (product configurations).
 *     responses:
 *       '200':
 *          description: Models.
 */ 
 router.get(
    '/models', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            const results = await checkout.getModels()
            res.send({ products: results })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

module.exports = router
