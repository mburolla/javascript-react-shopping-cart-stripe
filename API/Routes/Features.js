//
// File: Features.js
// Auth: Martin Burolla
// Date: 11/10/2022
// Desc: All the routes for /features/specs
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const checkout = require('../Checkout/Checkout')
const { handleError } = require('../Exceptions')

/**
 * @swagger
 * /features/specs:
 *   get: 
 *     tags:
 *       - Shopping
 *     description: Returns all the features and related specifications.
 *     responses:
 *       '200':
 *          description: Features and specifications.
 */ 
 router.get(
    '/features/specs', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            const results = await checkout.getFeaturesAndSpecs()
            res.send( results)
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

module.exports = router