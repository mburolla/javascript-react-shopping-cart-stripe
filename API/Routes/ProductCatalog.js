//
// File: ProductCatalog.js
// Auth: Marty Burolla
// Date: 12/19/2022
// Desc: Admin routes for maintaining the proudct catalog.
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const { handleError } = require('../Exceptions')
// const { body, check, param } = require('express-validator')
const productCatalogManager = require('../Managers/ProductCatalogManager')

/**
 * @swagger
 * /product-catalog:
 *   get: 
 *     tags:
 *       - Product Catalog
 *     description: Returns all of the product catalogs
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: Product catalogs.
 */ 
router.get(
    '/', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const productCatalogs = await productCatalogManager.getProductCatalogs()
            res.send({ productCatalogs })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /product-catalog/{productCatalogId}/products:
 *   get: 
 *     tags:
 *       - Product Catalog
 *     description: Returns all of the products for a product catalog.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: Product.
 */ 
router.get(
    '/:productCatalogId/products', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const productCatalogId = req.params['productCatalogId']
            const products = await productCatalogManager.getProducts(productCatalogId)
            res.send({ products })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /product-catalog/{productCatalogId}/products/{productId}:
 *   get: 
 *     tags:
 *       - Product Catalog
 *     description: Returns the details for a product.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: Product.
 */ 
router.get(
    '/:productCatalogId/products/:productId', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const productId = req.params['productId']
            const productCatalogId = req.params['productCatalogId']
            const product = await productCatalogManager.getProductDetails(productCatalogId, productId)
            res.send({ product })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)


/**
 * @swagger
 * /features:
 *   get: 
 *     tags:
 *       - Product Catalog
 *     description: Returns all of the features available in the Product Catalog.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: Features.
 */ 
router.get(
    '/features', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const features = await productCatalogManager.getFeatures()
            res.send({ features })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /specifications:
 *   get: 
 *     tags:
 *       - Product Catalog
 *     description: Returns all of the specifications.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: Specifications.
 */ 
router.get(
    '/features/:featureId/specifications', 
    common.readLimit, 
    common.requestDBLogger,
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const featureId = req.params['featureId']
            const specifications = await productCatalogManager.getSpecificationsForFeature(featureId)
            res.send({ specifications })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /products:
 *   post: 
 *     tags:
 *       - Product Catalog
 *     description: Add a new product.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: New product added.
 */ 
router.post(
    '/products', 
    common.readLimit, 
    common.requestDBLogger,
    // TODO: Add validation.
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const product = await productCatalogManager.addProduct(req.body)
            res.send(product)
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /products/feature:
 *   post: 
 *     tags:
 *       - Product Catalog
 *     description: Add a feature and specification to a product..
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: New feature/specification added.
 */ 
router.post(
    '/products/feature', 
    common.readLimit, 
    common.requestDBLogger,
    // TODO: Add validation.
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const productId = req.body.productId
            const featureId = req.body.featureId
            const specId = req.body.specificationId
            const productFeatureSpecId = await productCatalogManager.associateSpecWithFeature(productId, specId, featureId)
            res.send({productFeatureSpecId})
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /products/features:
 *   post: 
 *     tags:
 *       - Product Catalog
 *     description: Add a new feature.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: New feature added.
 */ 
router.post(
    '/products/features', 
    common.readLimit, 
    common.requestDBLogger,
    // TODO: Add validation.
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const description = req.body.description
            const featureId = await productCatalogManager.addFeature(description)
            res.send({featureId})
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /products/specifications:
 *   post: 
 *     tags:
 *       - Product Catalog
 *     description: Add a new specification.
 *     consumes:
 *        - application/json
 *     responses:
 *       '200':
 *          description: New specification added.
 */ 
router.post(
    '/products/specifications', 
    common.readLimit, 
    common.requestDBLogger,
    // TODO: Add validation.
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const description = req.body.description
            const featureId = req.body.featureId
            const specValue = req.body.value
            const cost = req.body.cost
            const specificationId = await productCatalogManager.addSpecification(featureId, description, specValue, cost)
            res.send({specificationId})
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

module.exports = router
