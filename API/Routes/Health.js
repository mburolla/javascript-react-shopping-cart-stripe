//
// File: Health.js
// Auth: Martin Burolla
// Date: 10/31/2022
// Desc: All the routes for /Health
//

const express = require('express')
const router = express.Router()

/**
 * @swagger
 * /health:
 *   get: 
 *     tags:
 *       - Utility
 *     description: Simple health check endpoint.
 *     responses:
 *       '200':
 *          description: OK.
 */ 
 router.get(
    '/health', 
    async (req, res) => {
      res.send("ok")
    }
)

module.exports = router
