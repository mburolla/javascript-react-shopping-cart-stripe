//
// File: Orders.js
// Auth: Martin Burolla
// Date: 10/31/2022
// Desc: All the routes for /Orders.
//

const express = require('express')
const router = express.Router()
const common = require('../Common')
const checkout = require('../Checkout/Checkout')
const { handleError } = require('../Exceptions')
const { body, check, param } = require('express-validator')

/**
 * @swagger
 * /orders/{orderUuid}/summary:
 *   get: 
 *     tags:
 *       - Checkout
 *     description: Returns the shipping information for the specified order.
 *     parameters:
 *       - in: path
 *         name: orderUuid
 *         required: true
 *         description: Order Id
 *         schema:
 *           type: string
 *           example: CF49F6B7
 *     responses:
 *       '200':
 *          description: Shipping name and address.
 *       '400':
 *          description: Validation errors.
 *       '404':
 *          description: OrderUuid not found.
 */ 
 router.get(
    '/orders/:orderUuid/summary',
    common.readLimit, 
    common.requestDBLogger,
    param('orderUuid').isLength({min: 8}), 
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const orderUuid = req.params.orderUuid
            const results = await checkout.getShippingInforForOrder(orderUuid)
            const items = await checkout.getItemsForOrder(orderUuid)
            const total = await checkout.getOrderAmount(orderUuid)
            res.send({ 
                shippingInfo: results,
                items: items,
                total: total 
            })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /orders:
 *    post:
 *      tags:
 *        - Checkout
 *      description: Creates a new order.
 *      consumes:
 *        - application/json
 *      parameters:
 *        - in: body
 *          name: Order Body
 *          description: Order information
 *          schema:
 *            type: object
 *            properties:
 *              shipping:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                    example: Martin J. Burolla
 *                  address:
 *                    type: object
 *                    properties:
 *                      line1:
 *                        type: string
 *                        example: 722 Ontario Drive
 *                      line2:
 *                        type: string
 *                        example: Apartment 2B
 *                      city:
 *                        type: string
 *                        example: Ontario
 *                      state:
 *                        type: string
 *                        example: NY
 *                      zip:
 *                        type: string
 *                        example: 14519
 *              items:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    qty:
 *                      type: integer
 *                      example: 1
 *                    modelId: 
 *                      type: integer
 *                      example: 1
 *      responses:
 *        '200':
 *           description: OrderUuid.
 *        '400':
 *          description: Validation errors.
 */
 router.post(
    '/orders', 
    common.writeLimit, 
    common.requestDBLogger,
    check('items').not().isEmpty(),
    body('shipping.name').isLength({ min: 3 }),
    body('shipping.address.line1').isLength({ min: 3 }),
    body('shipping.address.city').isLength({ min: 3 }),
    body('shipping.address.state').isLength({ min: 2 }),
    body('shipping.address.zip').isLength({ min: 5 }),
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const userId = common.getUserIdFromToken(req)
            const items = req.body.items
            const shipping = req.body.shipping
            const results = await checkout.createOrder(items, shipping, userId)
            res.send({ orderUuid: results })
        } 
        catch(error) {
            handleError(res, error)
        }
    }
)

/**
 * @swagger
 * /orders/status:
 *    put:
 *      tags:
 *        - Checkout
 *      description: Updates the order with the status of the Payment Intent from Stripe.
 *      consumes:
 *        - application/json
 *      parameters:
 *        - in: body
 *          name: orderUuid
 *          description: The order id to update
 *          schema:
 *            type: object
 *            required:
 *              - orderUuid
 *            properties:
 *              orderUuid:
 *                type: string
 *                example: CF49F6B7
 *              paymentIntentId:
 *                type: string
 *                example: pi_3LzjEvEazOekHy9O1Nr9tAhv
 *      responses:
 *        '200':
 *           description: Order status.
 *        '400':
 *          description: Validation errors.
 *        '404':
 *           description: OrderUuid not found.
 */
 router.put(
    '/orders/status', 
    common.writeLimit, 
    common.requestDBLogger,
    body('orderUuid').isLength({min: 8}),  
    body('paymentIntentId').isLength({min: 27}),
    async (req, res) => {
        try {
            common.validateRequest(req) // throws
            const orderUuid = req.body.orderUuid
            const paymentIntentId = req.body.paymentIntentId
            const results = await checkout.updateOrderStatus(orderUuid, paymentIntentId)
            res.send({
                orderUuid: orderUuid,
                status: results 
            })
        }
        catch(error) {
            handleError(res, error)
        }
    }
)
  
module.exports = router
