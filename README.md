# JavaScript React Shopping Cart Stripe
React application and Express web API that allows users to purchase products.  Integrates with [Stripe](https://stripe.com/) and
Auth0(https://www.auth0.com).

![](./docs/screen-shot-app.png)

![](./docs/screen-shot.png)

# Getting Started
- Create a Stripe account and copy your [Publishable and Secret keys](https://dashboard.stripe.com/test/apikeys)
- Clone this repo
- Install dependencies: `npm install`
- Create a MySql database called: `guitar-store`
- Create local files:
    - Create `/API/ConnString.js` based on [ConnString.template.js](./API/ConnString.template.js)
    - Create `/API/App.config.js` based on [App.config.js.template](./API/App.config.template.js)
    - Create `/API/db-migrate/database.json` based on [database.template.json](./API/db-migrate/database.template.json)
    - Create `/src/App.Config.js` based on [App.Config.js.template](./src/App.Config.template.js)
- Run db migration: `API/db-migrate/db-migrate up`
- Run the React App and Express Server: `npm start`
- View the app: [http://localhost:3000/](http://localhost:3000/)
- Browse the Swagger docs: [http://localhost:5150/api-docs/](http://localhost:5150/api-docs/)

# Overview
The `OrderUuid` from our Order System is stored in Stripe as a metadata field.  The Payment Intent ID from Stripe is stored in our Order System.  These two IDs link these two systems together.

![](./docs/integration.png)

Customers purchase Models.  A Model is a snapshot that represents a unique configuration of Features and Specifications.

# Checkout Overview

|Step|Context                   |Data                       |Our API              |Stripe SDK                        | 
|:--:|--------------------------|---------------------------|---------------------|----------------------------------|
|1   |Create order              |Shipping info              |POST /orders         |NA                                |
|2   |Start payment transaction |OrderUuid, order amount    |POST /payment-intents|`stripe.paymentIntents.create()`  |
|3   |Finish payment transaction|Payment Intent, credit card|NA                   |`stripe.confirmPayment()`         |
|4   |Get payment status        |Payment Intent             |PUT /orders/status   |`stripe.paymentIntents.retrieve()`|
|5   |Display order summary     |Order & Shipping info      |GET /orders/{id}/shipping<br />GET /orders/{id}/items |NA    |   

# Sequence Diagram: High Level

![](./docs/sd-stripe-high-level.png)

# Sequence Diagram: Detailed

![](./docs/sd-stripe-detail.png)

# Frontend

![](./docs/react-global-state-diagram.png)

# Backend API 
- [Swagger Docs](http://localhost:5150/api-docs/)
- [Mocha Tests](./API/Tests/test.js) Execute: `mocha`

![](./docs/swagger.png)

# API Architecture
API routes are stored [here](./API/Routes/).

![](./docs/api.png)

#### Database
---

![](./docs/erd.PNG)

[Database Doc](database-doc.md)

##### DB Migrate Notes
- Create migration: `db-migrate create <version name> --sql-file`
- Run migration: `db-migrate up`
- [DB Migrate](https://db-migrate.readthedocs.io/en/latest/Getting%20Started/commands/)

#### Backend Links
- [Server](./API/server.js)
- [DB Schema](./db-migrate/up-1.0.0.sql)
- [Express Validator](https://express-validator.github.io/docs/index.html)
- [Swagger API Docs](http://localhost:5150/api-docs/#/)
- [Postman 2xx Collection](./docs/Guitar%20Store%202xx.postman_collection.json)
- [Postman 4xx Collection](./docs/Guitar%20Store%204xx.postman_collection.json)
- [Rate Limiting](./API/App.config.template.js)

# Stripe
- https://stripe.com/docs/api
- https://dashboard.stripe.com/test/payments
- https://stripe.com/docs/api/payment_intents/object
- https://stripe.com/docs/api/payment_intents/confirm
- https://stripe.com/docs/api/payment_intents/create#create_payment_intent-shipping
- [Delete all test data](https://dashboard.stripe.com/test/developers)

# UPS
- https://www.ups.com/upsdeveloperkit?loc=en_US

# Swagger
- [JSON to SWAGGER YAML](https://json-to-swagger.netlify.app/) (life saver!)
- https://swagger.io/docs/specification/2-0/describing-request-body/
