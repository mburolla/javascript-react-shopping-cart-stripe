import React from 'react'
import { formatUSD } from './Util/Currency'
import './Products.scss'

export const Products = ({selectedProduct, models}) => {
    const onHandleChange = (productId) => {
        selectedProduct(productId)
    }

    return (
        <div className='Products'>
            {
                models &&
                <select onChange={e => onHandleChange(e.target.value)}>
                    <option value="">-- Select a guitar -- </option>
                    {
                        models.products &&
                        models.products.map(i => 
                            <option key={i.model_id} value={i.model_id}>{i.make} - {i.model} : {formatUSD(i.price)}  Model #{i.modelNumber}</option>
                        )
                    }
                </select>
            }
        </div>
    )
}
