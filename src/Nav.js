import React from 'react'
import { Link } from 'react-router-dom'
import { useAuth0 } from '@auth0/auth0-react'
import './Nav.scss'

export const Nav = () => {
  const { logout } = useAuth0()
  const { loginWithRedirect } = useAuth0()
  const { isAuthenticated } = useAuth0() // user

  const onHandleLogin = () => {
    loginWithRedirect()
  }

  return (
    <div className='Nav'>
        <div className='Nav_Nav'>
            <Link to='/'>Home</Link>&nbsp;&nbsp;
            <Link to='/shipping'>Checkout</Link>&nbsp;&nbsp;
            <Link to='/product-catalog'>Product Catalog</Link>
        </div>
        <div className='Nav_Title'>
            Shopping Cart
        </div>
        {
          !isAuthenticated && 
          <div className='Nav_Login'>
            <button onClick={() => onHandleLogin()}>Login</button>
          </div>
        }
        {
          isAuthenticated && 
          <div className='Nav_Login'>
             <button onClick={() => logout({ returnTo: window.location.origin })}>Logout</button>
          </div>
        }
    </div>
  )
}
