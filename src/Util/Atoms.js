// 
// File: Atoms.js
// Auth: Martin Burolla
// Date: 10/29/2022
// Desc: All the state for the application.
//

import { atom } from 'recoil'

//
// DESC: An array of items that a user wishes to purchase.
// NOTE: The modelId and qty are the only fields 
//       required for the call to POST /orders.
// [
//     {
//         "modelId: 1",
//         "qty": 2 ,
//         "make": "Suhr",
//         "model": "Modern",
//         "price": 1200.00
//     }
// ]
//
// USAGE:
//   ShippingInfo (Read)
//   OrderSummary (Reset)
//   Shopping (Read/Write)
//
export const shoppingCartItems = atom({
    key: 'shoppingCartItems',
    default: [] 
})
