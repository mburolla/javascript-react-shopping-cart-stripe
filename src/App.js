import React from "react"
import { Nav } from "./Nav"
import { HomePage } from "./Pages/HomePage"
import { PaymentPage } from "./Pages/PaymentPage"
import { SummaryPage } from "./Pages/SummaryPage"
import { ShippingPage } from "./Pages/ShippingPage"
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { ProductCatalogPage } from "./Pages/ProductCatalogPage"
import "./App.css"

export default function App() {

  return (
    <div className="App">
      <BrowserRouter>
          <Nav />
          <Routes>
            <Route path="/" exact={true} element={<HomePage />}/>
            <Route path="/shipping" element={<ShippingPage />}/>
            <Route path="/payment" element={<PaymentPage />}/>
            <Route path="/summary" element={<SummaryPage />}/>
            <Route path="/product-catalog" element={<ProductCatalogPage />}/>
          </Routes>
      </BrowserRouter>
    </div>
  )
}
