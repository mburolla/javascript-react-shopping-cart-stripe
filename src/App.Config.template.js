export const stripeRedirectUrl = 'http://localhost:3000/summary'
export const stripeKey = 'pk_test_xxx' // <=== Update with publishable key from Stripe.
export const apiBaseUrl = 'http://localhost:5150/'
export const domain   = 'auth0 domain' 
export const clientId = 'auth0 client id' 
export const audience = 'auth0 audience'
