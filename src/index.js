import App from "./App"
import React from "react"
import { RecoilRoot } from 'recoil'
import { createRoot } from 'react-dom/client'
import { Auth0Provider } from "@auth0/auth0-react"
import { QueryClient, QueryClientProvider } from 'react-query'
import { domain, audience, clientId } from './App.Config'

const queryClient = new QueryClient()
const container = document.getElementById('root')
const root = createRoot(container)

root.render(
  // <React.StrictMode> // Uncommenting this creates two Payment Intents in Stripe.
  <Auth0Provider 
    domain = { domain }
    clientId = { clientId }
    audience = { audience }
    redirectUri={ window.location.origin }
  >
    <RecoilRoot>
        <QueryClientProvider client={ queryClient }> 
            <App />
        </QueryClientProvider>
    </RecoilRoot>
  </Auth0Provider>
  // </React.StrictMode>
)
