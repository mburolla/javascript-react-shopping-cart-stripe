import React from 'react'
import { ProductCatalog } from '../Admin/ProductCatalog'

export const ProductCatalogPage = () => {
  return (
    <div>
        <ProductCatalog />
    </div>
  )
}
