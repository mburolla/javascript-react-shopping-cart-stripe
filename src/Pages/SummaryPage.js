import React from 'react'
import { Summary } from '../Checkout/Summary'

export const SummaryPage = () => {
    return (
        <div>
            <Summary />
        </div>
    )
}
