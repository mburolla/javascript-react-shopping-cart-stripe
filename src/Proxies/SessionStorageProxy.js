//
// File: SessionStorageProxy.js
// Auth: Martin Burolla
// Date: 10/19/2022
// Desc: The one and only interface into the browser's session storage.
//

const SS_GUITAR_STORE_KEY = 'Guitar_Store_V1'

export const initSessionStorageProxy = () => {
    sessionStorage.setItem(SS_GUITAR_STORE_KEY, JSON.stringify({
        orderUuid: ''
    }))
}

export const getOrderUuid = () => {
    let orderUuid = sessionStorage.getItem(SS_GUITAR_STORE_KEY)
    if (orderUuid == null) {
        sessionStorage.setItem(SS_GUITAR_STORE_KEY, JSON.stringify(
            {
                orderUuid: ''
            }))
            orderUuid = sessionStorage.getItem(SS_GUITAR_STORE_KEY)
    }
    return JSON.parse(orderUuid).orderUuid
}

export const setOrderUuid = (orderUuid) => {
    sessionStorage.setItem(SS_GUITAR_STORE_KEY, JSON.stringify({orderUuid: orderUuid}))
}
