//
// File: ApiProxy.js
// Auth: Martin Burolla
// Date: 10/14/2022
// Desc: The one and only interface to the web API.
//

import axios from 'axios'
import { apiBaseUrl } from '../App.Config'
import { useAuth0 } from '@auth0/auth0-react'

const BASE_URL = apiBaseUrl
const LOG_ENDPOINT = '/log'
const ORDERS_ENDPOINT = '/orders'
const MODELS_ENDPOINT = '/models'
const SEARCH_ENDPOINT = '/search'
const FEATURES_ENDPOINT = '/features/specs'
const UPDATE_ORDER_STATUS = '/orders/status'
const PAYMENT_INTENT_ENDPOINT = '/payment-intents'
const ORDERS_SHIPPING_ENDPOINT = `/orders/:id/summary`
//
const PRODUCT_CAT_FEATURES = '/product-catalogs/features'
const PRODUCT_CAT_SPECS = '/product-catalogs/features/:id/specifications'

const api = axios.create({ baseURL: BASE_URL })

export const useApiGetFeatures = () => {
    return async (headers) => {
        try {
            const res = await api.get(PRODUCT_CAT_FEATURES, headers)
            return res.data
        }
        catch (err) {
            console.log(err.response.data)
        }
    }
}

export const useApiGetSpecsForFeatureId = () => {
    return async (featureId, headers) => {
        try {
            const endpoint = PRODUCT_CAT_SPECS.replace(':id', featureId)
            const res = await api.get(endpoint, headers)
            return res.data
        }
        catch (err) {
            console.log(err.response.data)
        }
    }
}


//
// Reads
//

export const postModelsForSearch = async (query) => {
    try {
        const res = await api.post(SEARCH_ENDPOINT, { query })
        return res.data
    }
    catch(err) {
        console.log(query)
        console.log(err.response.data)
    }
}

export const useGetShippingInfo = () => {
    return async (orderUuid, headers) => {
        try {
            if (!orderUuid) {
                return null;
            }
            const endpoint = ORDERS_SHIPPING_ENDPOINT.replace(':id', orderUuid)
            const res = await api.get(endpoint, headers)
            return res.data
        }
        catch(err) {
            console.log(orderUuid)
            console.log(err.response.data)
        }
    }
}

export const getOrder = async (orderUuid) => {
    try {
        if (!orderUuid) {
            return null;
        }
        const res = await api.get(`${ORDERS_ENDPOINT}/${orderUuid}`)
        return res.data
    }
    catch(err) {
        console.log(orderUuid)
        console.log(err.response.data)
    }
}

export const getShowcaseModels = async (specIds) => {
    try {
        const res = await api.get(`${MODELS_ENDPOINT}`)
        return res.data
    }
    catch(err) {
        console.log(specIds)
        console.log(err.response.data)
    }
}

export const getFeatures = async () => {
    try {
        const res = await api.get(`${FEATURES_ENDPOINT}`)
        return res.data
    }
    catch(err) {
        console.log(err.response.data)
    }
}

//
// Writes
//

export const usePostOrder = () => {
    return async (shipping, items, headers) => {
        if (!shipping) {
            return
        }
        try {
            if (items.length === 0) {
                return
            }
            const res = await api.post(ORDERS_ENDPOINT, { shipping, items }, headers)
            return res.data
        }
        catch(err) {
            console.log(shipping)
            console.log(items)
            console.log(err.response?.data)
            console.log(headers)
        }
    }
}

export const usePaymentIntent = () => {
    return async (orderUuid, headers) => {
        if (!orderUuid) {
            return
        }
        try {
            const res = await api.post(PAYMENT_INTENT_ENDPOINT, {orderUuid}, headers)
            return res.data.clientSecret
        }
        catch(err) {
            console.log(orderUuid)
            console.log(err.response.data)
        }
    }
}

export const postLog = async (orderUuid, ip, message) => {
    try {
        const res = await api.post(`${LOG_ENDPOINT}`, {orderUuid, ip, message})
        return res.data
    }
    catch(err) {
        console.log(orderUuid)
        console.log(ip)
        console.log(message)
        console.log(err.response.data)
    }
}

export const useUpdatePaymentStatus = () => {
    return async (orderUuid, paymentIntentId, headers) => {
        try {
            if (!orderUuid) {
                return null;
            }
            const res = await api.put(`${UPDATE_ORDER_STATUS}`, {orderUuid, paymentIntentId: paymentIntentId}, headers)
            return res.data
        }
        catch(err) {
            console.log(orderUuid)
            console.log(paymentIntentId)
            console.log(err.response.data)
        }
    }
}

export const useCreateHeaders = async () => {
    const { getAccessTokenSilently } = useAuth0()
    const token = await getAccessTokenSilently()
    return {
        headers: {
          'Authorization': `Bearer ${token}`
        }
    }
}
