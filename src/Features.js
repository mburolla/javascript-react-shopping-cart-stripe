import React from 'react'
import { useQuery } from 'react-query'
import { getFeatures } from './Proxies/ApiProxy'
import './Features.scss'
import { Feature } from './Feature'

export const Features = ({checkedSpec, unCheckedSpec}) => {
    const featureQuery =  useQuery(`features`, async () => await getFeatures())

    return (
        <div className='Features'>
            {
                featureQuery.isFetched &&
                featureQuery.data.map(f => {
                    return <Feature 
                        key={f.featureId}
                        featureData={f}
                        checkedSpec={(specId) => checkedSpec(specId)}
                        unCheckedSpec={(specId) => unCheckedSpec(specId)}
                    />
                })
            }
        </div>
    )
}
