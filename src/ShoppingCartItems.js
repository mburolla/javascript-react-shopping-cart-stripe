import React from 'react'
import { formatUSD } from './Util/Currency'
import  './ShoppingCartItems.scss'

export const ShoppingCartItems = ({items, onHandleQtyChange, onHandleRemoveItem}) => {
    const total = (items.map(i => (i.price * i.qty))).reduce((a,b) => a + b, 0)
    return (
        <div className='ShoppingCartItems'>
        <table className='ShoppingCartItems_Table'>
                <tbody>
                    {
                        items.map(i => {
                            return <tr key={i.modelId}>
                                <td>{i.make} - {i.model}</td>
                                <td>({i.qty})</td>
                                <td>{formatUSD(i.price)} ea</td>
                                <td><button onClick={() => onHandleQtyChange({modelId: i.modelId, value: 1})}>+</button></td>
                                <td><button onClick={() => onHandleQtyChange({modelId: i.modelId, value: -1})}>-</button></td>
                                <td><button onClick={() => onHandleRemoveItem(i.productId)}>Remove</button></td>
                            </tr>
                        })
                    }
                </tbody>
        </table>
        <div className='ShoppingCartItems_Total'>
            Total: {' '}{formatUSD(total)}
        </div>
        </div>
    )
}
