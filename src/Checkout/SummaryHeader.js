import React from 'react'
import './SummaryHeader.scss'

export const SummaryHeader = ({orderUuid}) => {
  return (
    <div className='SummaryHeader'>
        <div className='SummaryHeader_Title'>
            Order Summary
        </div>
        <div className='SummaryHeader_Order'>
            Order ID: &nbsp;<div>{ orderUuid }</div>
        </div>
    </div>
  )
}
