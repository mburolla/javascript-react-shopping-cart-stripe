import React from 'react'
import { OrderSummary } from './OrderSummary'
import './Summary.scss'
import { useSearchParams } from 'react-router-dom'

export const Summary = ( ) => {
    const [searchParams] = useSearchParams()
    const paymentIntentId = searchParams.get('payment_intent') // This comes from the Stripe redirect callback.

    return (
        <div className='Summary'>
            <div>
                <OrderSummary paymentIntentId={paymentIntentId}/>
            </div>
        </div>
    )
}
