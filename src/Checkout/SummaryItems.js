import React from 'react'
import { formatUSD } from '../Util/Currency'
import './SummaryItems.scss'

export const SummaryItems = ({items, total}) => {
    return (
        <div className='SummaryItems'>
            <strong>Items:</strong><br />
            <table className="SummaryItems_Table">
                <tbody>
                    {
                        items.map(i => (
                            <tr key={i.order_item_id}>
                                <td>{i.make} - {i.model}</td> 
                                <td>{i.qty} x { formatUSD(i.price_each) } ea</td>
                                <td className='SummaryItems_TD_Right'>{formatUSD(i.total_price)}</td> 
                            </tr>
                        ))
                    }
                </tbody>
            </table>
            <div className="SummaryItems_Amount">
                Amount: { formatUSD(total) }
            </div>
        </div>
    )
}
