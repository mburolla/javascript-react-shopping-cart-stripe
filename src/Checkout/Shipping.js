import { useRecoilValue } from 'recoil'
import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { shoppingCartItems } from '../Util/Atoms'
import { usePostOrder, useCreateHeaders } from '../Proxies/ApiProxy'
import { getOrderUuid, setOrderUuid, initSessionStorageProxy } from '../Proxies/SessionStorageProxy'
import './Shipping.scss'

export const Shipping = () => {
    const navigate = useNavigate()
    const postOrder = usePostOrder()
    const headers = useCreateHeaders()
    const { register, handleSubmit, formState: { errors } } = useForm()
    const theShoppingCartItems = useRecoilValue(shoppingCartItems) // Read only.

    useEffect(() => {
        initSessionStorageProxy()
    }, [])
    
    useEffect(() => {
        if (theShoppingCartItems.length === 0 && getOrderUuid().length === 0) {
            // Arg, the user refeshed while viewin the ShippingPage.
            navigate("/")
        }
    }, [navigate, theShoppingCartItems])  

    const onSubmitForm = async (data) => {
        if (theShoppingCartItems.length === 0) { // User refreshed the page.
            return
        }
        const shippingInfo = {
            name: data.shipping_name,
            address: {
              line1: data.address_1,
              line2: data.address_2,
              city: data.city,
              state: data.state,
              zip: data.zip
            }
        }
        // No reason to send prices to the API.
        const purchaseItems = theShoppingCartItems.map(i => {
            return {
                modelId: i.modelId,
                qty: i.qty
            } 
        })
        const res = await postOrder(shippingInfo, purchaseItems, await headers)
        setOrderUuid(res.orderUuid)
        navigate("/payment")
    }

    return (
        <div className="Shipping">
            <form className="Shipping_Form" onSubmit={ handleSubmit(onSubmitForm) }>
                <input className="Shipping_Input"placeholder='Shipping Name' {...register("shipping_name",  { required: true })} />
                <input className="Shipping_Input" placeholder='Address 1' {...register("address_1", { required: true })} />
                <input className="Shipping_Input" placeholder='Address 2' {...register("address_2")} />
                <input className="Shipping_Input" placeholder='City' {...register("city", { required: true })} />
                <input className="Shipping_Input" placeholder='State' {...register("state", { required: true })} />
                <input className="Shipping_Input" placeholder='Zip' {...register("zip", { required: true })} />
                { errors.shipping_name && <><span className='ShippingInfo_Error'>Shipping name is required.</span><br/></> }
                { errors.address_1 && <><span className='ShippingInfo_Error'>Address 1 is required.</span><br/></> }
                { errors.city && <><span className='ShippingInfo_Error'>City is required.</span><br/></> }
                { errors.state && <><span className='ShippingInfo_Error'>State is required.</span><br/></> }
                { errors.zip && <><span className='ShippingInfo_Error'>Zip code is required.</span><br/></> }
                {
                    theShoppingCartItems.length === 0 &&
                    <span className='Shipping_Error'>
                        Cart is empty.
                    </span>
                }
                <input className="Shipping_Next" value="Next" type="submit"/>
            </form>
        </div>
    )
}
