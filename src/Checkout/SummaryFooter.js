import React from 'react'
import './SummaryFooter.scss'

export const SummaryFooter = () => {
  return (
    <div className="SummaryFooter">
        <button onClick={() => window.print()}>Print</button>
    </div>
  )
}
