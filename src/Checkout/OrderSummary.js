import { useQuery } from 'react-query'
import React, { useEffect } from 'react'
import { useResetRecoilState } from 'recoil'
import { SummaryItems } from './SummaryItems'
import { useNavigate } from 'react-router-dom'
import { SummaryHeader } from './SummaryHeader'
import { SummaryFooter } from './SummaryFooter'
import { shoppingCartItems } from '../Util/Atoms'
import { SummaryShipping } from './SummaryShipping'
import { getOrderUuid } from '../Proxies/SessionStorageProxy'
import { useUpdatePaymentStatus, useCreateHeaders, useGetShippingInfo } from '../Proxies/ApiProxy'
import './OrderSummary.scss'

export const OrderSummary = ({paymentIntentId}) => {
    const navigate = useNavigate()
    const orderUuid = getOrderUuid()
    const headers = useCreateHeaders()
    const shippingInfo = useGetShippingInfo()
    const updatePaymentStatus = useUpdatePaymentStatus()
    const resetCart = useResetRecoilState(shoppingCartItems) // Reset.
    useQuery(`paymentStatus/${orderUuid}`, async () => await updatePaymentStatus(orderUuid, paymentIntentId, await headers))
    const summaryQuery = useQuery(`shipping/${orderUuid}/summary`, async () => await shippingInfo(orderUuid, await headers))
    
    useEffect(() => {
        // Arg, the user tried to get here hackishly.
        if (orderUuid.length === 0 || !paymentIntentId) {
            navigate("/")
        }
    }, [orderUuid, navigate,paymentIntentId])

    useEffect(() => {
        resetCart() // Not really necessary b/c the callback from the Stripe redirect resets state.
    }, [orderUuid, resetCart])

    return (
        <div className='OrderSummary'>
            <div>
                {
                    summaryQuery.isFetched && 
                    <>
                        <SummaryHeader orderUuid={ orderUuid }/>
                        <SummaryShipping shipping={ summaryQuery.data.shippingInfo } />
                        <SummaryItems items={ summaryQuery.data.items } total={ summaryQuery.data.total }/>
                        <SummaryFooter />
                    </>
                }
            </div>
        </div>
    )
}
