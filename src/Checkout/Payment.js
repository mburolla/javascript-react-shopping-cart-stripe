import { useRecoilValue } from 'recoil'
import CheckoutForm from './CheckoutForm'
import { stripeKey } from '../App.Config.js'
import { useNavigate } from 'react-router-dom'
import { loadStripe } from '@stripe/stripe-js'
import { shoppingCartItems } from '../Util/Atoms'
import React, { useEffect, useState } from 'react'
import { Elements } from '@stripe/react-stripe-js'
import { getOrderUuid } from '../Proxies/SessionStorageProxy'
import { useCreateHeaders, usePaymentIntent } from '../Proxies/ApiProxy'
import './Payment.scss'

const stripePromise = loadStripe(stripeKey); // Make sure to call loadStripe outside of a component’s render

export const Payment = ( ) => {
    const navigate = useNavigate()
    const headers = useCreateHeaders()
    const paymentIntent = usePaymentIntent()
    let [clientSecret, setClientSecret] = useState(null)
    const theShoppingCartItems = useRecoilValue(shoppingCartItems) // Read only.

    useEffect(() => {
        if (theShoppingCartItems.length === 0) {
            // Arg, the user refeshed while viewing the Payment credit card page.
            navigate("/")
        }
    }, [theShoppingCartItems, navigate])  

    useEffect(() => {
        const fetchData = async () => {
            if (!clientSecret) {
                const orderId = getOrderUuid()
                const res = await paymentIntent(orderId, await headers)
                setClientSecret(res)
            }
        }
        fetchData();
    }, [headers, paymentIntent, clientSecret]) 

    return (
        <div className="Payment">
            {
                clientSecret && (
                <Elements 
                    options={ { clientSecret: clientSecret, appearance: { theme: 'stripe' } } } 
                    stripe={ stripePromise }>
                    <CheckoutForm orderUuid={getOrderUuid()}/>
                </Elements>)
            }
        </div>
    )
}
