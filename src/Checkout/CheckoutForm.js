import React, { useState } from "react"
import { stripeRedirectUrl } from "../App.Config"
import { useUpdatePaymentStatus, useCreateHeaders, postLog } from  '../Proxies/ApiProxy'
import { PaymentElement, useStripe, useElements } from "@stripe/react-stripe-js"
import './CheckoutForm.scss'

export default function CheckoutForm({orderUuid}) {
  const stripe = useStripe()
  const elements = useElements()
  const headers = useCreateHeaders()
  const updatePaymentStatus = useUpdatePaymentStatus()
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    setIsLoading(true)

    const clientIp = "NA"
    await postLog(orderUuid, clientIp, "APP: Confirm payment")
    const { error } = await stripe.confirmPayment({ // This adds the Payment Intent to the URL redirect_url.
      elements,
      confirmParams: {
        return_url: stripeRedirectUrl,
        receipt_email: email,
      }
    })

    // NOTE: This point is ONLY reached if there's an error, 
    // else the user is redirected to the return_url.
    await handleError(error)

    setIsLoading(false);
  }

  const handleError = async (error) => {
    if (error.type === "card_error" || error.type === "validation_error") {
      setMessage(error.message)
    } else {
      setMessage("An unexpected error occurred.")
    }

    await updatePaymentStatus(orderUuid, error.message, headers)
    await postLog(orderUuid, `confirmPayment ERROR: ${error.message}`)
  }

  return (
    <div className="CheckoutForm">
      <form id="payment-form" onSubmit={handleSubmit}>
        <input
          id="email"
          type="text"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Enter email address"
        />
        <PaymentElement id="payment-element" />
        <button disabled={isLoading || !stripe || !elements} id="submit">
          <span id="button-text">
            {isLoading ? <div className="spinner" id="spinner"></div> : "Pay now"}
          </span>
        </button>
        {message && <div id="payment-message">{message}</div>}
      </form>
    </div>
  )
}

//
// Code from Stripe's website:
//
// useEffect(() => {
//   async function fetchData() {
//     if (!stripe) {
//       return;
//     }
//     // Make sure to delete the URL when developing...
//     const clientSecret = new URLSearchParams(window.location.search).get("payment_intent_client_secret");
//     if (!clientSecret) {
//       return;
//     }
//     const res = await stripe.retrievePaymentIntent(clientSecret)
//     await postLog(orderUuid, `retrievePaymentIntent: ${res.paymentIntent.status}`)  
//     await updateOrderPaymentStatus(orderUuid, res.paymentIntent.status) 
//     if (res.paymentIntent.status !== "succeeded") {
//        setMessage("Something went wrong.")
//     }
//   }
//   fetchData();
// }, [stripe, orderUuid])