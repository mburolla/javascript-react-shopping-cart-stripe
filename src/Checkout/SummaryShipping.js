import React from 'react'
import './SummaryShipping.scss'

export const SummaryShipping = ({shipping}) => {
  return (
    <div className='SummaryShipping'>
        <strong>Ship To:</strong>
        <table className="SummaryShipping_Shipping">
            <tbody>
                <tr>
                    <td>{ shipping.shipping_name }</td>
                </tr>
                <tr>
                    <td>{ shipping.shipping_address_1 }</td>
                </tr>
                {
                    shipping.shipping_address_2.length > 0 && 
                    <tr>
                        <td>{ shipping.shipping_address_2 }</td>
                    </tr>
                }
                <tr>
                    <td>
                        { shipping.shipping_city }{ ' '} 
                        { shipping.shipping_state }, 
                        { shipping.shipping_zip }
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
  )
}
