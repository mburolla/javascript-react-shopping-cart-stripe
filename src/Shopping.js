import React, { useEffect, useState } from 'react'
import { Features } from './Features'
import { Products } from './Products'
import { useRecoilState } from 'recoil'
import { SpecTable } from './SpecTable'
import { postModelsForSearch } from './Proxies/ApiProxy'
import { shoppingCartItems } from './Util/Atoms'
import { ShoppingCartItems } from './ShoppingCartItems'
import { initSessionStorageProxy } from './Proxies/SessionStorageProxy'
import './Shopping.scss'

export const Shopping = () => {
    let [models, setModels] = useState([])
    let [model, setModel] = useState(null)
    let [searchQuery, setSearchQuery] = useState([])
    let [showSearch, setShowSearch] = useState(false)
    let [theShoppingCartItems, setShoppingCartItems] = useRecoilState(shoppingCartItems) // Read/Write.

    useEffect(() => {
      initSessionStorageProxy()
    }, [])

    useEffect(() => {
      const fetchData = async () => {
        if (searchQuery.length > 0) {
          const models = await postModelsForSearch(searchQuery)
          setModels(models)
        }
      }
      fetchData();
    }, [searchQuery]);

    const onHandleSelectedProduct = (modelId) => {
      const product = models.products.find(p => p.model_id === parseInt(modelId))
      const items = [...theShoppingCartItems]
      if (items.find(i => i.modelId === parseInt(modelId))){
        return // Do not add dupes to cart list.
      }
      setModel(product)
    } 

    const onHandleQtyChange = (model) => {
      let updateProduct = {...theShoppingCartItems.find(p => p.modelId === model.modelId)}
      if (model.value < 0 && updateProduct.qty === 1) {
        return;
      }
      updateProduct.qty = (updateProduct.qty + model.value)
      const items = [...theShoppingCartItems]
      let newArray = items.filter(i => i.modelId !== model.modelId)
      newArray.push(updateProduct)
      newArray.sort((a,b) => a.make.localeCompare(b.make))
      setShoppingCartItems(newArray)
    }

    const onHandleRemoveItem = (productId) => {
      const items = [...theShoppingCartItems]
      const newArray = items.filter(i => i.productId !== productId)
      setShoppingCartItems(newArray)
    }

    const onHandleAdd = () => {
      if (!model) {
        return
      }
      const items = [...theShoppingCartItems]
      items.push({
        productId: model.product_id,
        modelId: model.model_id,
        qty: 1,
        make: model.make,
        model: model.model,
        price: model.price
      })
      items.sort((a,b) => a.make.localeCompare(b.make))
      setShoppingCartItems(items)
    }

    const onHandleSearch = () => {
      setShowSearch(!showSearch)
    }

    const onHandleSelectedSpec = (featureSpec) => {
      const newSearchQuery = [...searchQuery]
      const featureNode = newSearchQuery.find(f => f.featureId === featureSpec.featureId)
      if (featureNode) {
        featureNode.specIds.push(featureSpec.specId)
      } 
      else {
        newSearchQuery.push({ // First time.
          featureId: featureSpec.featureId,
          specIds: [featureSpec.specId]
        })
      }
      setSearchQuery(newSearchQuery)
    }

    const onHandleUnSelectedSpec = (featureSpec) => {  
      let newSearchQuery = [...searchQuery]
      let featureNode = newSearchQuery.find(f => f.featureId === featureSpec.featureId)
      let index = featureNode.specIds.indexOf(featureSpec.specId)
      featureNode.specIds.splice(index, 1)
      if (featureNode.specIds.length === 0) {
        newSearchQuery = newSearchQuery.filter(i => i.featureId !== featureSpec.featureId)
      }
      setSearchQuery(newSearchQuery)
    }

    return (
        <div className="Shopping">
            <div className="Shopping_Selection">
                <Products models={models} selectedProduct={(productId) => onHandleSelectedProduct(productId)}/>
                <button onClick={() => onHandleAdd()}>Add to cart</button>
                <button onClick={() => onHandleSearch()}>Search</button>
            </div>
            {
                showSearch &&
                <>
                    <Features 
                      selectedSpec={(specId) => onHandleSelectedSpec(specId)} 
                      checkedSpec={(specId) => onHandleSelectedSpec(specId)}
                      unCheckedSpec={(specId) => onHandleUnSelectedSpec(specId)}
                    />
                </>
            }
            {
                model &&
                model.specs[0].spec_desc !== 'BASE' &&
                <>
                    <SpecTable specs={model.specs} />
                </>
            }
            {
                theShoppingCartItems.length > 0 &&
                <ShoppingCartItems 
                    items={theShoppingCartItems} 
                    onHandleQtyChange={(value) => onHandleQtyChange(value)}
                    onHandleRemoveItem={(value) => onHandleRemoveItem(value)}
                />
            }
        </div>
    )
}
