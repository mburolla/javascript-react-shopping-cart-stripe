import React from 'react'
import { formatUSD } from './Util/Currency'
import './Feature.scss'

export const Feature = ({featureData, checkedSpec, unCheckedSpec}) => {
    const onHandleClick = (target) => {
        if (target.checked) {
            checkedSpec({
                featureId: featureData.featureId,
                specId: Number(target.id)
            })
        } else {
            unCheckedSpec({
                featureId: featureData.featureId,
                specId: Number(target.id)
            })
        }
    }

    return (
        <div className='Feature'>
            <strong>{featureData.featureDesc}</strong><br/>
            <table>
                <thead>
                    <tr>
                        <td>Description</td>
                        <td>Value</td>
                        <td>Cost</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        featureData.specs.map(s => {
                            return <tr key={s.specificationId}>
                                <td className="Feature_Description">{s.description}</td>
                                <td className="Feature_Value">{s.value}</td>
                                <td>{formatUSD(s.cost)}</td>
                                <td><input id={s.specificationId} onClick={(e) => onHandleClick(e.target) } type='checkbox'></input></td>
                            </tr>
                        })
                    }
                </tbody>
            </table>
            <br/>
        </div>
    )
}
