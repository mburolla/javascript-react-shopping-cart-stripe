import React from 'react'
import './SpecTable.scss'
import { formatUSD } from './Util/Currency'

export const SpecTable = ({specs}) => {
    const displaySpecs = specs.filter(i=> i.spec_desc !== 'BASE')

    return (
        <div className='SpecTable'>
            <table>
                {
                    displaySpecs.length > 0 &&
                    <thead>
                        <tr>
                            <td>Feature</td>
                            <td>Option</td>
                            <td>Cost</td>
                        </tr> 
                    </thead>
                }
                <tbody>
                    {
                        displaySpecs.map(i=> (
                            <tr key={i.specification_id}>
                                <td>{i.feature_desc}</td>
                                <td>{i.spec_desc}</td>
                                <td>{formatUSD(i.cost)}</td>
                            </tr>))
                    }
                </tbody>
            </table>
        </div>
    )
}
