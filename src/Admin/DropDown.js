import React from 'react'
import './DropDown.scss'

export const DropDown = ({onSelected, dataSet, idIdentifier, displayText, defaultText}) => {

    const onHandleSelected = (value) => {
        if (!isNaN(value)) {
            onSelected(value)
        }
    }

    return (
        <div className="DropDown">
            <select onChange={(e) => {onHandleSelected(e.target.value)}}>
                <option defaultValue="-1">{defaultText}</option>
                {
                    dataSet.map(i => {
                        return  <option key={i[idIdentifier]} value={i[idIdentifier]}>{i[displayText]}</option>
                    })
                }
            </select>
        </div>
    )
}
