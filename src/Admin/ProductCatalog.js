import { DropDown } from './DropDown'
import { useQuery } from 'react-query'
import React from 'react'
import { 
    useCreateHeaders, 
    useApiGetFeatures,
    useApiGetSpecsForFeatureId
} from '../Proxies/ApiProxy'
import { SpecsTable } from './SpecsTable'

export const ProductCatalog = () => {
    let featureId = 0
    const apiGetFeatures = useApiGetFeatures()
    const apiGetSpecsForFeature = useApiGetSpecsForFeatureId()
    const headers = useCreateHeaders()
    const featuresQuery = useQuery(`features`, async () => await apiGetFeatures(await headers))
    const specsQuery = useQuery(`specsForFeature`, async () => await apiGetSpecsForFeature(featureId, await headers), {
        enabled: false,
        refetchOnWindowFocus: false
    })

    const onHandleSelect = (id) => {
        featureId = id
        specsQuery.refetch()
    }

    return (
        <div>
            {
                featuresQuery.isFetched && 
                <DropDown 
                    onSelected ={(id) => onHandleSelect(id)} 
                    dataSet = {featuresQuery.data.features}
                    idIdentifier = "feature_id"
                    displayText = "feature_desc" 
                    defaultText = "Features"
                />
            }
            {
                specsQuery.isFetched &&
                <SpecsTable data = {specsQuery.data.specifications}/>
            }
        </div>
    )
}
