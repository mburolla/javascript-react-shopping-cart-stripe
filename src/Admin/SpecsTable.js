import React from 'react'

export const SpecsTable = ({data}) => {
    return (
        <div>
            --- Specifications ---
            {
                data.map(s => {
                    return <div key={s.specification_id}>{s.feature_id}{' '}{s.spec_desc}{' '}{s.spec_value}{' '}{s.cost}</div>
                })
            } 
        </div>
    )
}
